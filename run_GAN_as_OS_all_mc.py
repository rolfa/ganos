
""" run_GAN_as_OS_all_mc - Run oversampling experiments for multiple predefined GAN configurations.

GAN configs that where pre-selected and saved to a CSV file in a specific format (see ana_of_cWGAN) are read and
processed. For each config that is found in the CSV file an experiment is performed where oversampling using a GAN
is compared to other oversampling strategies such as SMOTE. An evaluation is conducted through multiple classifiers
(Logistic Regression, Random Forest, Gradient Boosting) and evaluation measures (e.g. recall, precision); see the
oversampling script for more details. Also Logistic Regression and Gradient Boosting are used to try to distinguish
fake from real data (i.e. a probability of 50% is desirable). In the end all result are appended to a CSV file for and
easy overview.

The following helper functions are used in this script:
    * make_weights_from_imbratio - Transforms an imbalance ratio (e.g. 5 for 1:5) to a pair of weights
      (e.g. (.83, .16) ).

"""

import numpy as np
import pandas as pd
from helpers import calc_imb_ratio, get_elapsed_time, make_log_entry, create_artificial_data
from datasetloader import load_dataset
from oversampling import oversamp_comp0, make_pivot_table, predict_fake
from sklearn.model_selection import ParameterGrid
from datetime import datetime
import multiprocessing
import os
import shutil
from models import Generator
import torch

## pd.set_option('display.max_columns', None, 'display.width', 160)


def make_weights_from_imbratio(x):      ## transform to weights by: for imb_ratio x = [x/(x+1)] / [(1/(x+1)]
    a = round(x/(x+1),4)
    b = round(1/(x+1),4)
    return (a,b)


start_time = datetime.now().replace(microsecond=0)
tstamp = start_time.strftime("%Y-%m-%d_%H-%M-%S")


########################################################################################################################




load_config_file = 'GAN_exp_parameters.csv'

save_name2 = f'{tstamp}__run_GAN_as_OS_all_mc.csv'
save_log_name = f'{tstamp}__run_GAN_as_OS_all_mc.log'
save_res_name = f'{tstamp}__run_GAN_as_OS_all_mc_RESULTS'
num_cores = 28


os_methods = ['adasyn', 'smote', 'ros', 'nos', 'gan']
eval_methods = ['lr', 'rf', 'gb']
eval_measures = ['acc', 'f1_score', 'auc', 'precision', 'recall']

random_seed = 1
start_at = 0



params = pd.read_csv(load_config_file)

if start_at > 0 and start_at is not None: params = params.iloc[start_at:,:]
total_iter = len(list(params.index)) + start_at
cur_iter = start_at
make_log_entry(save_log_name, f'Script start:\t\t{start_time}\n\n')


for i in params.index:
    cur_iter += 1
    # if cur_iter == 2: break
    print(f'[{cur_iter} / {total_iter}]\t{get_elapsed_time(start_time)}')



    ## Consider for make_classification: 'n_classes * n_clusters_per_class must be smaller or equal 2 ** n_informative'
    if params.at[i, 'n_classes'] * params.at[i, 'n_clusters_per_class'] <= 2**params.at[i, 'n_informative']:

        load_dataset_params = {
            'dataset' : 'make',
            'normalize': False,
            'return_norm_vec': False,
            'vec_save': False,
            'test_split': None,
            'split_seed': 1,
            'make_params': {
                'n_samples': params.at[i,'n_samples'],
                'n_features': params.at[i, 'n_features'],
                'n_informative': params.at[i, 'n_informative'],
                'n_classes': params.at[i, 'n_classes'],
                'weights': make_weights_from_imbratio(params.at[i, 'imb_ratio2']),
                'n_clusters_per_class': params.at[i, 'n_clusters_per_class'],
                'flip_y': params.at[i, 'flip_y'],
                'hypercube': params.at[i, 'hypercube'],
                'random_state': params.at[i, 'random_state']
            }
        }


        DF = load_dataset(**load_dataset_params)
        cols = list(DF.columns[~DF.columns.isin(['target'])])
        num_feat = len(cols)



        GANconfig = {
            'num_epochs': 8000,
            # 'num_epochs': params.at[i, 'num_epochs'],
            'batch_size': int(params.at[i, 'batch_size']),
            'lr': float(params.at[i, 'lr']),
            'k_critic': int(params.at[i, 'k_critic']),
            'G': {
                'nz': int(params.at[i, 'nz']),
                'G_add_layers': int(params.at[i, 'G_add_layers']),
                'nGh0': int(params.at[i, 'nGh0']),
                'nGh1': int(params.at[i, 'nGh1']),
                'nGh2': int(params.at[i, 'nGh2']),
                'nGh3': int(params.at[i, 'nGh3']),
            },
            'D': {
                'D_add_layers': int(params.at[i, 'D_add_layers']),
                'nDh0': int(params.at[i, 'nDh0']),
                'nDh1': int(params.at[i, 'nDh1']),
                'nDh2': int(params.at[i, 'nDh2']),
                'nDh3': int(params.at[i, 'nDh3']),
            },
            'random_seed': 1,
            'print_progress':True,
            'save_params': True,
            'save_results': True,
            # 'report_epoch_interval': 10,
            'report_epoch_interval': 100,
            'num_workers': 1,
            'ngpu': 0,
        }



        rf_grid = {
            'n_estimators': [500],
            'max_features': list(np.arange(1,int(round(np.sqrt(num_feat))))),
            'bootstrap': ['True', 'False']
        }

        rf_grid_iter = len(ParameterGrid(rf_grid))


        gb_grid = {
            'max_depth': [2, 3, 4, 5, 6],
            'n_estimators': [1000],
            'learning_rate': [0.05, 0.1, 0.5, 1],
            'subsample': [.2, .5, .8],
            'n_iter_no_change': [50],
            'validation_fraction': [0.1],
            'tol': [0.0001]
        }

        gb_grid_iter = len(ParameterGrid(gb_grid))



        res_te, params_te = oversamp_comp0(
                                            dataset=DF,
                                            target = 'target',
                                            save_results = '',
                                            cv_k = 3,
                                            test_size = 0.2,
                                            os_methods = os_methods,
                                            eval_methods = eval_methods,
                                            eval_measures = eval_measures,
                                            random_seed = random_seed,
                                            device = None,
                                            rf_grid = rf_grid,
                                            rf_grid_iter = min(rf_grid_iter, 100),
                                            gb_grid = gb_grid,
                                            gb_grid_iter = min(gb_grid_iter, 100),
                                            grid_verbose= 1,
                                            return_conf = True,
                                            return_best_params = True,
                                            GANconfig=GANconfig,
                                            GANtype='cWGAN',
                                            num_cores = min(multiprocessing.cpu_count(), num_cores),
                                            print_progress = True)



        ## results from oversampling to df
        naming = pd.Series(res_te.index).str.extract(r'(?P<eval_method>.+)_(?P<os_method>.+)_(?P<eval_measure>.+)')
        res = pd.DataFrame(res_te, columns=['result'])
        res = pd.concat([res.reset_index(drop=True), naming.reset_index(drop=True)], axis=1)
        piv = pd.pivot_table(res, values='result', index=['os_method'], columns=['eval_method','eval_measure'])
        piv.columns = [f'{i}_{j}' for i,j in piv.columns]
        piv.insert(loc=0, column='OS', value=piv.index)

        piv2 = pd.pivot_table(res, values='result', index=['os_method', 'eval_method'], columns=['eval_measure'])
        osvals = [x[0] for x in list(piv2.index)]
        methvals = [x[1] for x in list(piv2.index)]
        piv2.insert(loc=0, column='OS', value=osvals)
        piv2.insert(loc=1, column='ALG', value=methvals)

        ## Add best parameters
        naming_params = pd.Series(params_te.index).str.extract(r'(?P<ALG>.+)_(?P<OS>.+)_.*_.*')
        params_te2 = pd.DataFrame(params_te, columns=['best_params']).reset_index(drop=True)
        params_te2 = pd.concat([naming_params, params_te2], axis=1)


        ## parameters to df
        param_tab = load_dataset_params['make_params'].copy()
        param_tab['imb_ratio'] = round(calc_imb_ratio(DF), 1)
        param_tab['imb_ratio2'] = int(round(param_tab['weights'][0] / param_tab['weights'][1]))
        param_tab['weights'] = str(param_tab.get('weights'))
        param_tab1 = pd.DataFrame(pd.DataFrame.from_dict(param_tab, dtype='str', orient='index').transpose())
        param_tab1.insert(loc=9, column='n_informative2', value=param_tab['n_informative'] / param_tab['n_features'])
        param_tab2 = pd.DataFrame()
        for j in range(piv2.shape[0]):
            param_tab2 = pd.concat([param_tab2, param_tab1], axis=0)

        save_tab2 = pd.concat([param_tab2.reset_index(drop=True), piv2.reset_index(drop=True)], axis=1)
        save_tab2 = pd.merge(save_tab2, params_te2, on=['OS', 'ALG'], how='left')


        ## predict fake
        G_load_path2 = 'G_params.pt'
        MMloadpath2 = 'MinMax.npy'
        G = Generator(ngpu=0, num_features=num_feat, **GANconfig['G'], conditional=True)
        G.load_state_dict(torch.load(G_load_path2, map_location='cpu'))
        G.eval()

        cols = list(DF.columns)[:-1]
        t0_size = DF[DF.target == 0].shape[0]
        t1_size = DF[DF.target == 1].shape[0]
        rDF = DF.copy()
        rDF['fake'] = 0
        rDF1 = rDF[rDF.target == 1].drop('target', axis=1)
        rARTU = create_artificial_data(G=G, t0_size=t0_size, t1_size=t1_size, cols=cols, unnormLOAD=MMloadpath2)
        rARTU['fake'] = 1
        rARTU1 = create_artificial_data(G=G, t0_size=0, t1_size=t1_size, cols=cols, unnormLOAD=MMloadpath2)
        rARTU1 = rARTU1.drop('target', axis=1)
        rARTU1['fake'] = 1
        rDF = pd.concat([rDF.reset_index(drop=True), rARTU.reset_index(drop=True)], axis=0)
        rDF1 = pd.concat([rDF1.reset_index(drop=True), rARTU1.reset_index(drop=True)], axis=0)

        fake_res, fake_params = predict_fake(       ## We reuse the grids for RF and GB from above
            DF=rDF,
            target='fake',
            test_size=0.2,
            eval_methods=['lr', 'gb'],
            # eval_methods=['lr'],
            return_conf=False,
            cv_k=3,
            gb_grid=gb_grid,
            gb_grid_iter=gb_grid_iter,
            rf_grid=rf_grid,
            rf_grid_iter=rf_grid_iter,
            grid_verbose=1,
            grid_scorer='accuracy_score',
            num_cores=min(multiprocessing.cpu_count(), num_cores),
            return_best_params=True
        )

        fake_res1, fake_params1 = predict_fake(
            DF=rDF1,
            target='fake',
            test_size=0.2,
            eval_methods=['lr', 'gb'],
            # eval_methods=['lr'],
            return_conf=False,
            cv_k=3,
            gb_grid=gb_grid,
            gb_grid_iter=gb_grid_iter,
            rf_grid=rf_grid,
            rf_grid_iter=rf_grid_iter,
            grid_verbose=1,
            grid_scorer='accuracy_score',
            num_cores=min(multiprocessing.cpu_count(), num_cores),
            return_best_params=True
        )

        fake_pred = pd.DataFrame({
            'OS': ['GAN'],
            'LR_fake_predict': [fake_res['LR_GAN_acc']],
            'GB_fake_predict': [fake_res['GB_GAN_acc']],
            'LR_fake_predict1': [fake_res1['LR_GAN_acc']],
            'GB_fake_predict1': [fake_res1['GB_GAN_acc']],
        })

        save_tab2 = pd.merge(save_tab2, fake_pred, on=['OS'], how='left')



        ## Add avg_JS0 from random search
        JS0 = pd.DataFrame({
            'JS0': [params.at[i, 'avg_JS0']],
            'OS': 'GAN'
        })
        save_tab2 = pd.merge(save_tab2, JS0, on=['OS'], how='left')


        ## Load GAN results
        gan_res = pd.read_csv('results.csv')
        gan_res = gan_res.tail(1)
        gan_res.drop('Unnamed: 0', axis=1, inplace=True)
        gan_res['epoch'] = gan_res['epoch'] + 1
        gan_res['OS'] = 'GAN'
        save_tab2 = pd.merge(save_tab2, gan_res, on=['OS'], how='left')


        ## Add GANconfig
        gan_conf = pd.DataFrame({
            'num_epochs': [params.at[i, 'num_epochs']],
            'batch_size': [int(params.at[i, 'batch_size'])],
            'lr': [float(params.at[i, 'lr'])],
            'k_critic': [int(params.at[i, 'k_critic'])],
            'nz': [int(params.at[i, 'nz'])],
            'G_add_layers': [int(params.at[i, 'G_add_layers'])],
            'nGh0': [int(params.at[i, 'nGh0'])],
            'nGh1': [int(params.at[i, 'nGh1'])],
            'nGh2': [int(params.at[i, 'nGh2'])],
            'nGh3': [int(params.at[i, 'nGh3'])],
            'D_add_layers': [int(params.at[i, 'D_add_layers'])],
            'nDh0': [int(params.at[i, 'nDh0'])],
            'nDh1': [int(params.at[i, 'nDh1'])],
            'nDh2': [int(params.at[i, 'nDh2'])],
            'nDh3': [int(params.at[i, 'nDh3'])],
            'OS': 'GAN',
            'load_path': [params.at[i, 'load_path']],
            'exp_num': [params.at[i, 'exp_num']]
        })
        save_tab2 = pd.merge(save_tab2, gan_conf, on=['OS'], how='left')


        ## Add dataset conf for easy orientation
        save_tab2.insert(loc=0, column='dataset', value=params.at[i, 'dataset'])
        save_tab2.index = [cur_iter] * save_tab2.shape[0]


        with open(save_name2, 'a') as f:
            save_tab2.to_csv(f, header=f.tell() == 0, index=True)



        ## save params to separate folder, so I can easily load and use it later
        param_save_folder = 'run_GAN_as_OS_Param_Saves'
        if not os.path.exists(param_save_folder):
            os.makedirs(param_save_folder)

        nf = params.at[i, 'n_features']
        ir = params.at[i, 'imb_ratio2']
        nc = params.at[i, 'n_clusters_per_class']
        ninf = str(params.at[i, 'n_informative'] / params.at[i, 'n_features']).replace('.','')
        bs = GANconfig['batch_size']
        lr = round(GANconfig['lr'], 5)
        k = GANconfig['k_critic']
        nz = GANconfig['G']['nz']
        nGh0 = GANconfig['G']['nGh0']
        nGh1 = GANconfig['G'].get('nGh1', 0)
        nGh2 = GANconfig['G'].get('nGh2', 0)
        nGh3 = GANconfig['G'].get('nGh3', 0)
        nDh0 = GANconfig['D']['nDh0']
        nDh1 = GANconfig['D'].get('nDh1', 0)
        nDh2 = GANconfig['D'].get('nDh2', 0)
        nDh3 = GANconfig['D'].get('nDh3', 0)

        Gname = f"G_params_{nf}_{ir}_{nc}_{ninf}___cWGAN__{bs}_{lr}_{k}__{nz}_{nGh0}_{nGh1}_{nGh2}_{nGh3}" \
                 "__{nDh0}_{nDh1}_{nDh2}_{nDh3}"
        MinMaxname = f"MinMax_{nf}_{ir}_{nc}_{ninf}___cWGAN__{bs}_{lr}_{k}__{nz}_{nGh0}_{nGh1}_{nGh2}_{nGh3}" \
                      "__{nDh0}_{nDh1}_{nDh2}_{nDh3}"
        shutil.copy2(src=f'G_params.pt', dst=param_save_folder)
        shutil.copy2(src=f'MinMax.npy', dst=param_save_folder)
        os.rename(src=f'{param_save_folder}/G_params.pt', dst=f'{param_save_folder}/{Gname}.pt')
        os.rename(src=f'{param_save_folder}/MinMax.npy', dst=f'{param_save_folder}/{MinMaxname}.npy')



        ## Print to logfile
        cur_data = f"{param_tab['n_features']}_{param_tab['imb_ratio2']}_{param_tab['n_clusters_per_class']}"
        cur_data = f"{cur_data}_{str(param_tab['n_informative'] / param_tab['n_features']).replace('.','')}"
        Logprint = f'[{cur_iter} / {total_iter}]\t{get_elapsed_time(start_time)}\t\t{cur_data}\n'
        make_log_entry(save_log_name, Logprint)


        ## Print pivot tables to text file
        Logprint = f'## [{cur_iter} / {total_iter}]\t{get_elapsed_time(start_time)}\t\t{cur_data}\n\n'
        make_log_entry(save_res_name, Logprint)
        Logprint = f"{make_pivot_table(round(res_te,3))}\n\n"
        make_log_entry(save_res_name, Logprint)
        Logprint = f"predict_fake\n{make_pivot_table(round(fake_res, 3))}\n\n"
        make_log_entry(save_res_name, Logprint)
        Logprint = f"predict_fake1\n{make_pivot_table(round(fake_res1, 3))}\n\n\n\n\n\n"
        make_log_entry(save_res_name, Logprint)



print('---------------\nTotal time:')
print(datetime.now().replace(microsecond=0) - start_time)


Logprint = f"\n\n---------------\nScript ends:\t{datetime.now().replace(microsecond=0)}"
make_log_entry(save_log_name, Logprint)
Logprint = f"\nTotal Time:\t{get_elapsed_time(start_time)}"
make_log_entry(save_log_name, Logprint)









