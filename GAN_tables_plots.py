""" GAN_tables_plots - Plot the final result tables

From a specified set of experiment results (csv - obtained from run_GAN_as_OS_all_mc) means with confidence intervals
are plotted over different dataset characteristics, oversampling methods and classifiers.
Also, Friedman tests whether rankings over the different datasets significantly differ between oversampling methods
are performed.

"""


import pandas as pd
from matplotlib import pyplot as plt
import seaborn as sns
from scipy.stats import friedmanchisquare

pd.set_option('display.max_columns', None, 'display.width', 160)


load_file = 'FULL_difficult.csv'
# load_file = 'FULL_easy.csv'


## Load file and created comprehensive dataset
DF = pd.read_csv(load_file, header=0)
Xcols = ['n_features', 'imb_ratio2', 'n_clusters_per_class', 'n_informative2']
cols = ['n_features', 'imb_ratio2', 'n_clusters_per_class', 'n_informative2', 'f1', 'recall', 'precision', 'auc', 'acc']
cols = ['n_features', 'imb_ratio2', 'n_clusters_per_class', 'f1', 'recall', 'precision', 'auc', 'acc']
cols += ['OS' ,'ALG']
DF = DF[cols]
groupbyList = ['n_features', 'imb_ratio2', 'n_clusters_per_class', 'ALG']
DF['rank_f1'] = DF.groupby(groupbyList)['f1'].rank(method='min', ascending=False)
DF['rank_recall'] = DF.groupby(groupbyList)['recall'].rank(method='min', ascending=False)
DF['rank_precision'] = DF.groupby(groupbyList)['precision'].rank(method='min', ascending=False)
DF['rank_auc'] = DF.groupby(groupbyList)['auc'].rank(method='min', ascending=False)
DF['dataset_id'] = DF.groupby(groupbyList).ngroup().add(1)
DF0 = DF.copy()
DF2 = DF.copy()
DF2['ALG'] = 'Overall'
DF = pd.concat([DF, DF2], axis=0, ignore_index=True)
DF['OS'] = pd.Categorical(DF['OS'], categories=['NOS', 'ROS', 'SMO', 'ADA', 'GAN'])  ## This will determine ordering
DF['ALG'] = pd.Categorical(DF['ALG'], categories=['LR', 'RF', 'GB', 'Overall'])


## Friedman test on ranks for each ALG and evaluation measure
fried = pd.DataFrame()
for cur_clf in ['LR', 'RF', 'GB', 'Overall']:
    DFclf = DF[DF['ALG'] == cur_clf].sort_values(['dataset_id', 'OS'])
    DFclf = DFclf[[c for c in DFclf.columns if 'rank' in c] + ['OS', 'dataset_id']]

    for cur_measure in [c for c in DFclf.columns if 'rank' in c]:
        cur_list = [ DFclf[[cur_measure]][DFclf['OS'] == os].values.ravel() for os in DFclf['OS'].unique() ]
        teststat, pval = friedmanchisquare(*cur_list)
        cur_res = pd.DataFrame({'ALG': [cur_clf],
                                'measure': [cur_measure],
                                'test_statistic': [teststat],
                                'p_value': [pval]})
        fried = pd.concat([fried, cur_res], axis=0, ignore_index=True)


## Get all means and rankings
DFmeans = DF.groupby(['ALG','OS'])['f1', 'recall', 'precision', 'auc',
                                   'rank_f1', 'rank_recall','rank_precision', 'rank_auc'].mean()
DFmeans = round(DFmeans,2)
DFmeans[[c for c in DFmeans.columns if 'rank' in c]] = round(DFmeans[[c for c in DFmeans.columns if 'rank' in c]],1)
DFsd = round(DF.groupby(['ALG','OS'])['f1', 'recall', 'precision', 'auc'].std(),2)
DFmeans.insert(loc=4, column='SD_auc', value=DFsd['auc'])
DFmeans.insert(loc=3, column='SD_precision', value=DFsd['precision'])
DFmeans.insert(loc=2, column='SD_recall', value=DFsd['recall'])
DFmeans.insert(loc=1, column='SD_f1', value=DFsd['f1'])
print(DFmeans)
print(fried)


## Graphic: Mean with CIs for measures (3 rows) over ALG (3 cols), OS within plot
DFbp = DF0.copy()
DFbp['ALG'] = pd.Categorical(DFbp['ALG'], categories=['LR', 'RF', 'GB'])
DFbp['OS'] = pd.Categorical(DFbp['OS'], categories=['NOS', 'ROS', 'SMO', 'ADA', 'GAN'])
# DFbp = DF[DF['OS'].isin(['GAN', 'SMO', 'NOS'])]

sns.set_style('whitegrid')
fig, ax = plt.subplots(nrows=3, ncols=3, figsize=(10, 12))
measure = 'precision'
# measure = 'recall'
ALGlist = ['LR', 'RF', 'GB']
paramlist = ['n_features', 'imb_ratio2', 'n_clusters_per_class']
sns.set_style('whitegrid')

for j in range(3):
    sns.pointplot(x='n_features', y=measure, hue='OS', data=DFbp[DFbp['ALG'] == ALGlist[j]], kind='point',
                  errstyle='bars', ci=95, join=False, capsize=.05, dodge=.4, errwidth=1, legend=False,
                  markers=['s','^','o','D','x'], scale=0.7, palette=['black'], ax=ax[0][j])
    sns.pointplot(x='imb_ratio2', y=measure, hue='OS', data=DFbp[DFbp['ALG'] == ALGlist[j]], kind='point',
                errstyle='bars', ci=95, join=False, capsize=.05, dodge=.4, errwidth=1, legend=False,
                markers=['s','^','o','D','x'], scale=0.7, palette=['black'], ax=ax[1][j])
    sns.pointplot(x='n_clusters_per_class', y=measure, hue='OS',data=DFbp[DFbp['ALG'] == ALGlist[j]], kind='point',
                errstyle='bars', ci=95, join=False, capsize=.05, dodge=.4, errwidth=1, legend=False,
                markers=['s','^','o','D','x'], scale=0.7, palette=['black'], ax=ax[2][j])

sns.despine(left=True, bottom=True)
for i in range(3):
    ax[i][1].set(yticklabels=[])
    ax[i][2].set(yticklabels=[])
    for j in range(3):
        ax[i][j].get_legend().set_visible(False)
        ax[i][j].set_xlabel("")
        ax[i][j].set_ylabel("")
        ax[i][j].set_ylim(-0.01, 1.01)


ax[0][1].set_xlabel("Number of Features", fontsize=13)
ax[1][1].set_xlabel("Imbalance Ratio", fontsize=13)
ax[2][1].set_xlabel("Data Complexity", fontsize=13)
# ax[0][0].set_ylabel(measure, fontsize=15)
ax[1][0].set_ylabel(measure, fontsize=15)
# ax[2][0].set_ylabel(measure, fontsize=15)
ax[0][0].set_title('Logistic Regression', fontsize=13)
ax[0][1].set_title('Random Forest', fontsize=13)
ax[0][2].set_title('Gradient Boosting', fontsize=13)
ax[0][2].legend(loc='best', frameon=False)
plt.subplots_adjust(left=0.08, bottom=0.08, right=0.98, top=0.92, hspace=0.40, wspace=0.05)

plt.savefig(f'avgCI_{measure}.pdf')
plt.show()
