"""mc_regression - run regressions on scores to check whether dataset characteristcs have an effect on outcome

Uses standardized regression coefficients to tackle the question which specific dataset characteristics have an
effect on the scores under different combinations of oversampling strategies and classifiers.
Note that, thus, the strength of the effects cannot be compared directly among different regressions, but show whether
specific dataset characteristics have an effect in several cases or not.

"""

import pandas as pd
import statsmodels.formula.api as smf           ## allows R style regression formulas
from scipy.stats import zscore

pd.set_option('display.max_columns', None, 'display.width', 160)


## Load datasets
load_file = '~/Desktop/mc_3000/Full_mc_3000.csv'
DF = pd.read_csv(load_file, header=0)

## Calculate all regressions and print results
standardize = True

if standardize is True:
    formula_f1 = 'zscore(f1) ~ zscore(n_features) + zscore(imb_ratio2) + zscore(n_clusters_per_class) ' \
              '+ zscore(n_informative2) -1'
    formula_recall = 'zscore(recall) ~ zscore(n_features) + zscore(imb_ratio2) + zscore(n_clusters_per_class) ' \
                 '+ zscore(n_informative2) -1'
    formula_precision = 'zscore(precision) ~ zscore(n_features) + zscore(imb_ratio2) + zscore(n_clusters_per_class) ' \
                 '+ zscore(n_informative2) -1'
else:
    formula_f1 = 'f1 ~ n_features + imb_ratio2 + n_clusters_per_class + n_informative2'
    formula_recall = 'f1 ~ n_features + imb_ratio2 + n_clusters_per_class + n_informative2'
    formula_precision = 'f1 ~ n_features + imb_ratio2 + n_clusters_per_class + n_informative2'


DFalg = DF[DF['ALG'] == 'LR']
res = smf.ols(formula_f1, data=DFalg[DFalg['OS'] == 'NOS']).fit()
print(res.summary())
res = smf.ols(formula_recall, data=DFalg[DFalg['OS'] == 'NOS']).fit()
print(res.summary())
res = smf.ols(formula_precision, data=DFalg[DFalg['OS'] == 'NOS']).fit()
print(res.summary())

res = smf.ols(formula_f1, data=DFalg[DFalg['OS'] == 'ROS']).fit()
print(res.summary())
res = smf.ols(formula_recall, data=DFalg[DFalg['OS'] == 'ROS']).fit()
print(res.summary())
res = smf.ols(formula_precision, data=DFalg[DFalg['OS'] == 'ROS']).fit()
print(res.summary())

res = smf.ols(formula_f1, data=DFalg[DFalg['OS'] == 'SMO']).fit()
print(res.summary())
res = smf.ols(formula_recall, data=DFalg[DFalg['OS'] == 'SMO']).fit()
print(res.summary())
res = smf.ols(formula_precision, data=DFalg[DFalg['OS'] == 'SMO']).fit()
print(res.summary())

res = smf.ols(formula_f1, data=DFalg[DFalg['OS'] == 'ADA']).fit()
print(res.summary())
res = smf.ols(formula_recall, data=DFalg[DFalg['OS'] == 'ADA']).fit()
print(res.summary())
res = smf.ols(formula_precision, data=DFalg[DFalg['OS'] == 'ADA']).fit()
print(res.summary())


DFalg = DF[DF['ALG'] == 'RF']
res = smf.ols(formula_f1, data=DFalg[DFalg['OS'] == 'NOS']).fit()
print(res.summary())
res = smf.ols(formula_recall, data=DFalg[DFalg['OS'] == 'NOS']).fit()
print(res.summary())
res = smf.ols(formula_precision, data=DFalg[DFalg['OS'] == 'NOS']).fit()
print(res.summary())

res = smf.ols(formula_f1, data=DFalg[DFalg['OS'] == 'ROS']).fit()
print(res.summary())
res = smf.ols(formula_recall, data=DFalg[DFalg['OS'] == 'ROS']).fit()
print(res.summary())
res = smf.ols(formula_precision, data=DFalg[DFalg['OS'] == 'ROS']).fit()
print(res.summary())

res = smf.ols(formula_f1, data=DFalg[DFalg['OS'] == 'SMO']).fit()
print(res.summary())
res = smf.ols(formula_recall, data=DFalg[DFalg['OS'] == 'SMO']).fit()
print(res.summary())
res = smf.ols(formula_precision, data=DFalg[DFalg['OS'] == 'SMO']).fit()
print(res.summary())

res = smf.ols(formula_f1, data=DFalg[DFalg['OS'] == 'ADA']).fit()
print(res.summary())
res = smf.ols(formula_recall, data=DFalg[DFalg['OS'] == 'ADA']).fit()
print(res.summary())
res = smf.ols(formula_precision, data=DFalg[DFalg['OS'] == 'ADA']).fit()
print(res.summary())


DFalg = DF[DF['ALG'] == 'GB']
res = smf.ols(formula_f1, data=DFalg[DFalg['OS'] == 'NOS']).fit()
print(res.summary())
res = smf.ols(formula_recall, data=DFalg[DFalg['OS'] == 'NOS']).fit()
print(res.summary())
res = smf.ols(formula_precision, data=DFalg[DFalg['OS'] == 'NOS']).fit()
print(res.summary())

res = smf.ols(formula_f1, data=DFalg[DFalg['OS'] == 'ROS']).fit()
print(res.summary())
res = smf.ols(formula_recall, data=DFalg[DFalg['OS'] == 'ROS']).fit()
print(res.summary())
res = smf.ols(formula_precision, data=DFalg[DFalg['OS'] == 'ROS']).fit()
print(res.summary())

res = smf.ols(formula_f1, data=DFalg[DFalg['OS'] == 'SMO']).fit()
print(res.summary())
res = smf.ols(formula_recall, data=DFalg[DFalg['OS'] == 'SMO']).fit()
print(res.summary())
res = smf.ols(formula_precision, data=DFalg[DFalg['OS'] == 'SMO']).fit()
print(res.summary())

res = smf.ols(formula_f1, data=DFalg[DFalg['OS'] == 'ADA']).fit()
print(res.summary())
res = smf.ols(formula_recall, data=DFalg[DFalg['OS'] == 'ADA']).fit()
print(res.summary())
res = smf.ols(formula_precision, data=DFalg[DFalg['OS'] == 'ADA']).fit()
print(res.summary())
