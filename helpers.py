
"""helpers - A collection of useful functions

This is a collection of some useful functions that are used in the other scripts.
It contains the following functions:

    * plot_dists - plot and save the distribution of real data against fake data for visual comparison of the quality
      of the learned data distribution.
    * icr - calculate the interquartile range for an array of numbers.
    * freedman_diaconis_bins - calculate number of hist bins using Freedman-Diaconis rule (default of sns.distplot).
    * calc_distdist - returns the distance between two distributions based on bins.
    * scale_MinMax - apply MinMax scaling to a dataset (i.e. a strategy to normalize to [0,1] range).
    * unscale_MinMax - reverse MinMax scaling to original scale.
    * create_artificial_data - create artificial data from a trained Generator.
    * make_trainable_tensor_data - brings a dataset to a tensor format ready for GAN training.
    * load_G_from_path - load a saved (and trained) Generator.
    * wang_KL - implementation of an algorithm to approximate the KL divergence by
      Wang et al. (2009): Divergence Estimation for Multidimensional Densities Via k-Nearest-Neighbor Distances.
    * wang_KL_both - apply the above algorithm in both directions simultaneously to save time (since KL asymmetric).
    * calc_emp_cov - calculate the empirical covariance matrix of a dataframes.
    * calc_frobenius_on_diff - calculate the Frobenius norm on the difference of matrices..
    * calc_frobenius_on_diff_cov - calculate the Frobenius norm on the difference of the Cov matrices of two dataframes.
    * calc_imb_ratio - calculate the imbalance ratio from the majority class to the minority class.
    * get_from_config_for_train - get a predefined set from the config file of a Neural Network.
    * add_from_config_for_train - get some config parameters from an existing config and add to another config.
    * get_syn_data_from_OS - extract the synthetic data only from an already oversampled dataset.
    * get_frobenius_for_OS - calc Frobenius norm for synthetic data only from an already oversampled dataset.
    * get_elapsed_time - get the elapsed time since some previously defined starting time.
    * make_log_entry - make an entry to a logfile to track progress of scripts.

"""



import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
import seaborn as sns
import sys
from scipy.spatial.distance import jensenshannon        ## Note: if no probs are given then normalizes automatically
from scipy.stats import entropy
import torch
from torch import Tensor
from models import Generator
import json
import copy
from scipy.spatial import distance_matrix
from scipy import stats
from datetime import datetime



def plot_dists(dataR, dataA, columns=None, target='target', hist=True, kde=True, rug=False, save_name=None, show=True,
               title_pre = None, show_measure=None, norm_hist=False, hist_bins='freedman'):

    if isinstance(columns, list):
        num_cols = len(columns)
        dataR = dataR.loc[:, dataR.columns.isin(columns + [target])]
    else:
        num_cols = len(dataR.columns) - 1
        columns = dataR.columns

    if num_cols > 12:
        raise Exception('Too many columns specified. Max is 12.')


    fig, ax = plt.subplots(nrows=3, ncols=num_cols, figsize=(14, 6))
    cond_majorR = np.where(dataR[target] == 0)[0].tolist()
    cond_minorR = np.where(dataR[target] == 1)[0].tolist()
    cond_majorA = np.where(dataA[target] == 0)[0].tolist()
    cond_minorA = np.where(dataA[target] == 1)[0].tolist()


    for j in range(num_cols):
        sns.distplot(dataR.loc[:, columns[j]], hist=hist, kde=kde, rug=rug, norm_hist=norm_hist,
                     color='darkgreen', ax=ax[0][j], label='Real')
        sns.distplot(dataA.loc[:, columns[j]], hist=hist, kde=kde, rug=rug, norm_hist=norm_hist,
                     color='darkred', ax=ax[0][j], label='Fake')

        sns.distplot(dataR.loc[:, columns[j]][cond_majorR], hist=hist, kde=kde, rug=rug, norm_hist=norm_hist,
                     color='darkgreen', ax=ax[1][j])
        sns.distplot(dataA.loc[:, columns[j]][cond_majorA], hist=hist, kde=kde, rug=rug, norm_hist=norm_hist,
                     color='darkred', ax=ax[1][j])

        sns.distplot(dataR.loc[:, columns[j]][cond_minorR], hist=hist, kde=kde, rug=rug, norm_hist=norm_hist,
                     color='darkgreen', ax=ax[2][j])
        sns.distplot(dataA.loc[:, columns[j]][cond_minorA], hist=hist, kde=kde, rug=rug, norm_hist=norm_hist,
                     color='darkred', ax=ax[2][j])


        ## Set same xlims for Plots on vertical axis, i.e. for one Var
        x_min = min(dataR.loc[:, columns[j]].min(), dataA.loc[:, columns[j]].min())
        x_max = max(dataR.loc[:, columns[j]].max(), dataA.loc[:, columns[j]].max())

        clist = [[0,1],[0],[1]]
        for i in range(3):
            ax[i][j].set_xlim(x_min, x_max)
            if show_measure in ('KL', 'JS', 'intersect'):
                measure = calc_distdist(dataR[dataR.target.isin(clist[i])].iloc[:, j],
                                        dataA[dataA.target.isin(clist[i])].iloc[:, j],
                                        bins=hist_bins, measure=show_measure)
                ax[i][j].set_xlabel(f'{show_measure}: {round(measure,3)}', fontsize=9)
            elif show_measure in ('KL_both'):
                kl1, kl2 = wang_KL_both(dataR[dataR.target.isin(clist[i])].iloc[:, j],
                                        dataA[dataA.target.isin(clist[i])].iloc[:, j],
                                        whitening=True)
                ax[i][j].set_xlabel(f'KL1: {round(kl1, 3)};  KL2: {round(kl2, 3)}', fontsize=7)
            else:
                ax[i][j].set_xlabel("")

            ax[i][j].xaxis.set_tick_params(labelsize=6)
            ax[i][j].yaxis.set_tick_params(labelsize=6)


        ## Add Var labels and overwrite xlabels at the bottom with ""
        #ax[2][j].set_xlabel("")
        ax[0][j].set_title(dataR.columns[j], fontsize=9)

    ## Add title and Row labels
    ax[0][0].legend(loc='best', frameon=False)    ## Plot legend in first Plot only
    plt.suptitle(f"{title_pre}Distributions of real and fake data over variables", fontsize=20)
    ax[0][0].set_ylabel("All classes", fontsize=9, rotation=0, horizontalalignment='right')
    ax[1][0].set_ylabel("Majority", fontsize=9, rotation=0, horizontalalignment='right')
    ax[2][0].set_ylabel("Minority", fontsize=9, rotation=0, horizontalalignment='right')

    # plt.tight_layout()
    plt.subplots_adjust(left=0.08, bottom=0.08, right=0.98, top=0.89, wspace=.40, hspace=0.30)

    if save_name != None:
        plt.savefig(save_name)

    if show == True:
        plt.show()
    else:
        plt.close()

    pass




## From   https://github.com/mwaskom/seaborn/blob/master/seaborn/utils.py
def iqr(a):
    a = np.asarray(a)
    q1 = stats.scoreatpercentile(a, 25)
    q3 = stats.scoreatpercentile(a, 75)
    return q3 - q1




## From distplot source code:   https://github.com/mwaskom/seaborn/blob/master/seaborn/distributions.py
def freedman_diaconis_bins(a):
    ## From https://stats.stackexchange.com/questions/798/
    a = np.asarray(a)
    if len(a) < 2:
        return 1
    h = 2 * iqr(a) / (len(a) ** (1 / 3))
    # fall back to sqrt(a) bins if iqr is 0
    if h == 0:
        return int(np.sqrt(a.size))
    else:
        return int(np.ceil((a.max() - a.min()) / h))




def calc_distdist(dataR, dataF, bins='freedman', measure='JS'):

    if bins == 'sqrt':
        bins = int(round(np.sqrt(max(dataR.shape[0], dataF.shape[0])),0))

    if bins == 'freedman':                    ## this mehtod is used by default by sns.distplot for calculations of bins
        bins = freedman_diaconis_bins(dataR)  ## Use range of dataR for both to have same support

    Rhist, _ = np.histogram(dataR, bins=bins)
    Fhist, _ = np.histogram(dataF, bins=bins)
    Rhist = Rhist / np.sum(Rhist)
    Fhist = Fhist / np.sum(Fhist)

    if measure == 'JS':                       ## Note that this is mathematically not a valid approximation to the real
        dist = jensenshannon(Rhist, Fhist)    ## JS. However, this is a very pragmatic and efficient approximation that
                                              ## is sufficient to track the progress of the quality of the fake data.
    elif measure == 'KL':                     ## Anyway, in the end this is used along with other evaluation measures.
        dist = entropy(Rhist, Fhist)          ## Same is true for the KL

    elif measure == 'intersect':
            dist = 0
            for i in range(Rhist.shape[0]):         ## Note: This assumes that Rhist and Fhist have the same length
                dist += min(Rhist[i], Fhist[i])

    else:
        raise Exception("measure unknown. Allowed are: 'JS', 'KL'")

    return dist




def scale_MinMax(data, save_name=None, return_minmax = False, deep_copy = True):

    if deep_copy: data = copy.deepcopy(data)

    if 'target' in data.columns:
        cols = data.columns[~data.columns.isin(['target'])]
    else:
        cols = data.columns

    if isinstance(data, pd.DataFrame):
        dfmin = data[cols].min(axis=0).values
        dfmax = data[cols].max(axis=0).values
    else:
        dfmin = data[cols].min(axis=0)
        dfmax = data[cols].max(axis=0)

    data[cols] = (data[cols] - dfmin) / (dfmax - dfmin)

    if not save_name is None:
        mm = np.stack((dfmin, dfmax), axis=1)
        np.save(f'{save_name}', mm)

    if return_minmax:
        minmax = np.stack((dfmin, dfmax), axis=1)
    else:
        minmax = None

    return data if not return_minmax else (data, minmax)




def unscale_MinMax(data, load_name=None, minmax=None, deep_copy = True):

    if load_name:
        mm = np.load(load_name)
    elif not (minmax is None):
        mm = minmax
    else:
        raise Exception("Specify either load_name or minmax.")

    if 'target' in data.columns:
        cols = data.columns[~data.columns.isin(['target'])]
    else:
        cols = data.columns

    if data[cols].shape[1] != mm.shape[0]:
        sys.exit(f'Input data shape does not fit MinMAx; columns needed: {mm.shape[0]}')

    if deep_copy: data = copy.deepcopy(data)

    dfmin = mm[:, 0]
    dfmax = mm[:, 1]

    data[cols] = data[cols] * (dfmax - dfmin) + dfmin

    return data





def create_artificial_data(G, t0_size=0, t1_size=0, cols=None, unnormVECS=None, unnormLOAD=None, manual_seed=None,
                           device=None):

    if device is None:
        device = torch.device('cpu')     ## can use GPU or CPU; CPU is default. Raises error if not specified correctly

    if not (unnormVECS is None or unnormLOAD is None):
        raise Exception("Specify either 'unnormVECS' or 'unnormLOAD' or none of them. But not both.")

    if manual_seed is not None and isinstance(manual_seed, int):
        if next(G.parameters()).is_cuda:
            torch.cuda.manual_seed_all(manual_seed)
        else:
            torch.manual_seed(manual_seed)

    nz = G.__getattribute__('ninput') - 1
    num_feat = G.__getattribute__('num_features')

    if cols is None:
        cols = np.arange(num_feat)


    if t0_size > 0:
        noise0 = torch.randn(t0_size, nz).to(device)
        t0 = torch.zeros([t0_size, 1]).to(device)
        with torch.no_grad():
            example0 = G(noise0, t0).detach().cpu() ##to(device)

        ART0data = pd.DataFrame(example0.numpy(), columns=cols)

        if (not unnormVECS is None) and (unnormLOAD is None):
            ART0data = unscale_MinMax(ART0data, minmax=unnormVECS)

        if (unnormVECS is None) and not (unnormLOAD is None):
            ART0data = unscale_MinMax(ART0data, load_name=unnormLOAD)

        ART0data['target'] = np.zeros(t0_size, dtype=int)


    if t1_size > 0:
        noise1 = torch.randn(t1_size, nz).to(device)
        t1 = torch.ones([t1_size, 1]).to(device)
        with torch.no_grad():
            example1 = G(noise1, t1).detach().cpu()

        ART1data = pd.DataFrame(example1.numpy(), columns=cols)

        if (not unnormVECS is None) and (unnormLOAD is None):
            ART1data = unscale_MinMax(ART1data, minmax=unnormVECS)

        if (unnormVECS is None) and not (unnormLOAD is None):
            ART1data = unscale_MinMax(ART1data, load_name=unnormLOAD)

        ART1data['target'] = np.ones(t1_size, dtype=int)

    if t0_size > 0 and t1_size > 0:
        ARTdata = pd.concat([ART0data, ART1data], ignore_index=True)

    elif t1_size == 0:
        ARTdata = ART0data

    elif t0_size == 0:
        ARTdata = ART1data

    return ARTdata




def make_trainable_tensor_data(data, target='target'):
    Tdata = torch.tensor(data.loc[:, data.columns != target].values).float()
    Ttarget = torch.tensor(data.loc[:, data.columns == 'target'].values).long()
    ret_data = [[Tdata[i], Ttarget[i]] for i in range(len(Ttarget))]
    return ret_data




def load_G_from_path(load_path, num_features, set_eval=True, G_load_name='G_params.pt',
                     G_arch_load_name = 'params.json'):

    with open(f'{load_path}/{G_arch_load_name}') as f:
        params = json.load(f)

    G_arch = params['G']
    G = Generator(ngpu=0, num_features=num_features, **G_arch)
    G.load_state_dict(torch.load(f'{load_path}/{G_load_name}', map_location='cpu'))
    if set_eval:
        G.eval()

    return G







def wang_KL(df1, df2, k=1, add_noise=True, noise_upper=0.0001, whitening=True):

    df1 = pd.DataFrame(df1)
    df2 = pd.DataFrame(df2)

    if 'target' in df1.columns: df1 = df1.drop('target', axis=1)
    if 'target' in df2.columns: df2 = df2.drop('target', axis=1)

    n1, d1 = pd.DataFrame(df1).shape
    n2, d2 = pd.DataFrame(df2).shape

    ## Add noise to avoid division by zero
    if add_noise:
        df1 = df1 + np.random.uniform(0, noise_upper, df1.shape)
        df2 = df2 + np.random.uniform(0, noise_upper, df2.shape)


    if whitening:                 ## https://gist.github.com/joelouismarino/ce239b5601fff2698895f48003f7464b
        df12 = pd.concat([df1, df2], axis=0).values
        df12_c = df12 - np.mean(df12, axis=0)
        Sigma = np.dot(df12_c.T, df12_c) / df12_c.shape[0]         ## ZCA: 'zero-phase component analysis'
        U, lamb, V = np.linalg.svd(Sigma)
        W = np.dot(U, np.dot(np.diag(1.0 / np.sqrt(lamb + 1e-5)), U.T))
        df12_w = np.dot(df12_c, W.T)

        df12_w = pd.DataFrame(df12_w)
        df1 = df12_w.iloc[0:n1, :]
        df2 = df12_w.iloc[n1:(n1+n2),:]

    df1_dists = distance_matrix(df1, df1, p=2)
    cross_dists = distance_matrix(df1, df2, p=2)


    rhos = np.partition(df1_dists, k).T[k]        ## Note: take k for index, since the smallest value at 0 is always 0.0
    nus = np.partition(cross_dists, k).T[k-1]     ## (i.e. dist to oneself)
    D = (d1 / n1) * np.sum(np.log(nus/rhos)) + np.log(n2 / (n1 - 1))

    return D




def wang_KL_both(df1, df2, k=1, add_noise=True, noise_upper=0.0001, whitening=True):
                            ## This is only a small advantage in time over running 'wang_KL' twice, but at least ~25%
    df1 = pd.DataFrame(df1)
    df2 = pd.DataFrame(df2)

    if 'target' in df1.columns: df1 = df1.drop('target', axis=1)
    if 'target' in df2.columns: df2 = df2.drop('target', axis=1)

    n1, d1 = pd.DataFrame(df1).shape
    n2, d2 = pd.DataFrame(df2).shape

    if add_noise:
        df1 = df1 + np.random.uniform(0, noise_upper, df1.shape)
        df2 = df2 + np.random.uniform(0, noise_upper, df2.shape)

    if whitening:
        df12 = pd.concat([df1, df2], axis=0).values
        df12_c = df12 - np.mean(df12, axis=0)
        Sigma = np.dot(df12_c.T, df12_c) / df12_c.shape[0]
        U, lamb, V = np.linalg.svd(Sigma)
        W = np.dot(U, np.dot(np.diag(1.0 / np.sqrt(lamb + 1e-5)), U.T))
        df12_w = np.dot(df12_c, W.T)

        df12_w = pd.DataFrame(df12_w)
        df1 = df12_w.iloc[0:n1, :]
        df2 = df12_w.iloc[n1:(n1+n2),:]

    df1_dists = distance_matrix(df1, df1, p=2)
    df2_dists = distance_matrix(df2, df2, p=2)
    cross_dists = distance_matrix(df1, df2, p=2)

    rhos1 = np.partition(df1_dists, k).T[k]
    rhos2 = np.partition(df2_dists, k).T[k]
    nus1 = np.partition(cross_dists, k).T[k-1]
    nus2 = np.partition(cross_dists.T, k).T[k-1]
    D1 = (d1 / n1) * np.sum(np.log(nus1/rhos1)) + np.log(n2 / (n1 - 1))
    D2 = (d2 / n2) * np.sum(np.log(nus2/rhos2)) + np.log(n1 / (n2 - 1))

    return D1, D2







def calc_emp_cov(X):
    return np.dot(X.T, X) / X.shape[0]





def calc_frobenius_on_diff(df1, df2, triangular=True, k=-1, normalize=False):
    if triangular:
        df1 = np.tril(df1, k=k)
        df2 = np.tril(df2, k=k)

    if normalize:
        df1 /= np.sum(df1)
        df2 /= np.sum(df2)

    R = df1 - df2
    return np.linalg.norm(R, ord='fro')





def calc_frobenius_on_diff_cov(df1, df2, triangular=True, k=-1, normalize=False):
    C1 = calc_emp_cov(df1)
    C2 = calc_emp_cov(df2)
    return calc_frobenius_on_diff(C1, C2, triangular=triangular, k=k, normalize=normalize)




def calc_imb_ratio(df, target='target'):
    return round(df[target].value_counts()[0] / df[target].value_counts()[1], 2)




def get_from_config_for_train(config):
    res = {}
    paramList = ['num_epochs', 'batch_size', 'k_critic', 'lr', 'D', 'G']
    for p in paramList:
        res[p] = config[p]
    return res



def add_from_config_for_train(config, add_to_config):
    res = add_to_config
    paramList = ['num_epochs', 'batch_size', 'k_critic', 'lr', 'D', 'G']
    if config.get('pause_G_start') is not None: paramList += ['pause_G_start']
    if config.get('pause_G_during') is not None: paramList += ['pause_G_during']
    for p in paramList:
        res[p] = config[p]
    return res




def get_syn_data_from_OS(Xorig, Xos):
    nsyn = Xos.shape[0] - Xorig.shape[0]
    Xsyn = Xos.iloc[-nsyn:,:]
    return Xsyn


def get_frobenius_for_OS(Xorig, yorig, Xos, triangular=True, k=-1, normalize=False):
    Xsyn = get_syn_data_from_OS(Xorig, Xos)
    return calc_frobenius_on_diff_cov(Xorig[yorig.target == 1], Xsyn, triangular=triangular, k=k, normalize=normalize)



def get_elapsed_time(start_time):
    return datetime.now().replace(microsecond=0) - start_time



def make_log_entry(save_name, entry):
    with open(save_name, 'a') as f:
        f.write(entry)
