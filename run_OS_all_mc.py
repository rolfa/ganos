
""" run_OS_all_mc - Run oversampling experiments on a set of synthetic datasets (without GANs).

This script is used to conduct oversampling experiments on a large set of different synthetic datasets. Dataset
characteristics are specified and for all combinations of these characteristics synthetic datasets are created. For
each of these datasets several 'classical' oversampling methods are applied and evalutated using different classifiers
and evaluation measures. Note that no GAN oversampling is performed here.
All results are appended systematically to an CSV file. The results are subsequently used to figure out which dataset
characteristics are sensitive to different oversampling strategies and classifiers (e.g. whether the performance of
SMOTE suffers with an increasing number of variables using Gradient Boosting). See mc_regressions for more details.

The following helper functions are used in this script:
    * make_weights_from_imbratio - Transforms an imbalance ratio (e.g. 5 for 1:5) to a pair of weights
      (e.g. (.83, .16) ).

"""


import numpy as np
import pandas as pd
from helpers import calc_imb_ratio, get_elapsed_time, make_log_entry
from datasetloader import load_dataset
from oversampling import oversamp_comp0
from sklearn.model_selection import ParameterGrid
from datetime import datetime
import multiprocessing

## pd.set_option('display.max_columns', None, 'display.width', 160)


def make_weights_from_imbratio(x):      ## transform to weights by: for imb_ratio x = [x/(x+1)] / [(1/(x+1)]
    a = round(x/(x+1),4)
    b = round(1/(x+1),4)
    return (a,b)



start_time = datetime.now().replace(microsecond=0)
tstamp = start_time.strftime("%Y-%m-%d_%H-%M-%S")


save_name2 = f'{tstamp}__run_OS_all_mc.csv'
save_log_name = f'{tstamp}__run_OS_all_mc.log'


os_methods = ['adasyn', 'smote', 'ros', 'nos']
eval_methods = ['lr', 'rf', 'gb']
eval_measures = ['acc', 'f1_score', 'auc', 'precision', 'recall']

random_seed = 1
start_at = 0


n_samples = 5000
n_features = [10, 20, 30, 40, 50, 60, 70,80, 90, 100]
n_informativ = [0.3, 0.4, 0.5, 0.6, 0.7, 0.8]
n_redundant = 0
n_repreated = 0
imb_ratios = [5, 10, 15, 20, 25, 30, 35, 40, 45, 50]
weights =list(map(make_weights_from_imbratio, imb_ratios))
n_clusters_per_class = [1,2,3,4,5]
flip_y = 0.01
hypercube = [True]
class_sep = 1.0
shift = 0.0
scale = 1.0




datagrid = {                                                    ## Use Z, X etc. to determine order of Parametergrid
    'Zn_features': n_features,
    'Yweights': weights,
    'Xn_clusters_per_class': n_clusters_per_class,
    # 'Wflip_y': flip_y,
    'Vn_informative': n_informativ,
    'Uhypercube': hypercube,
}

grid = ParameterGrid(datagrid)
if start_at > 0 and start_at is not None: grid = list(grid)[start_at:]
total_iter = len(grid) + start_at
cur_iter = start_at
make_log_entry(save_log_name, f'Script start:\t\t{start_time}\n\n')


for params in grid:
    cur_iter += 1
    # if cur_iter == 1: break
    print(f'[{cur_iter} / {total_iter}]\t{get_elapsed_time(start_time)}')

    params['n_samples'] = n_samples
    params['n_classes'] = 2
    params['random_state'] = random_seed
    params['n_features'] = params.pop('Zn_features')
    params['weights'] = params.pop('Yweights')
    params['n_clusters_per_class'] = params.pop('Xn_clusters_per_class')
    params['flip_y'] = flip_y
    params['hypercube'] = params.pop('Uhypercube')
    params['n_informative'] = params.pop('Vn_informative')
    params['n_informative'] = int(np.round(params['n_features'] * params['n_informative']))
    params['n_redundant'] = n_redundant
    params['n_repeated'] = n_repreated
    params['class_sep'] = class_sep
    params['shift'] = shift
    params['scale'] = scale


    ## Consider for make_classification: 'n_classes * n_clusters_per_class must be smaller or equal 2 ** n_informative'
    if params['n_classes'] * params['n_clusters_per_class'] <= 2**params['n_informative']:

        load_dataset_params = {
            'dataset' : 'make',
            'normalize': False,
            'return_norm_vec': False,
            'vec_save': False,
            'test_split': None,
            'split_seed': 1,
            'make_params': {
                'n_samples': params['n_samples'],
                'n_features': params['n_features'],
                'n_informative': params['n_informative'],
                'n_classes': params['n_classes'],
                'weights': params['weights'],
                'n_clusters_per_class': params['n_clusters_per_class'],
                'flip_y': params['flip_y'],
                'hypercube': params['hypercube'],
                'random_state': params['random_state']
            }
        }


        DF = load_dataset(**load_dataset_params)
        cols = list(DF.columns[~DF.columns.isin(['target'])])
        num_feat = len(cols)


        rf_grid = {
            'n_estimators': [500],
            'max_features': list(np.arange(1,int(round(np.sqrt(num_feat))))),
            'bootstrap': ['True', 'False']
        }

        rf_grid_iter = len(ParameterGrid(rf_grid))


        gb_grid = {
            'max_depth': [2, 3, 4, 5, 6],
            'n_estimators': [1000],
            'learning_rate': [0.05, 0.1, 0.5, 1],
            'subsample': [.2, .5, .8],
            'n_iter_no_change': [50],
            'validation_fraction': [0.1],
            'tol': [0.0001]
        }

        gb_grid_iter = len(ParameterGrid(gb_grid))



        res_te, params_te = oversamp_comp0(
                                            dataset=DF,
                                            target = 'target',
                                            save_results = '',
                                            cv_k = 3,
                                            test_size = 0.2,
                                            os_methods = os_methods,
                                            eval_methods = eval_methods,
                                            eval_measures = eval_measures,
                                            random_seed = random_seed,
                                            device = None,
                                            rf_grid = rf_grid,
                                            rf_grid_iter = min(rf_grid_iter, 100),
                                            gb_grid = gb_grid,
                                            gb_grid_iter = min(gb_grid_iter, 100),
                                            grid_verbose= 1,
                                            return_conf = True,
                                            return_best_params = True,
                                            num_cores = min(multiprocessing.cpu_count(), 28),
                                            print_progress = True)



        ## results from oversampling to df
        naming = pd.Series(res_te.index).str.extract(r'(?P<eval_method>.+)_(?P<os_method>.+)_(?P<eval_measure>.+)')
        res = pd.DataFrame(res_te, columns=['result'])
        res = pd.concat([res.reset_index(drop=True), naming.reset_index(drop=True)], axis=1)
        piv = pd.pivot_table(res, values='result', index=['os_method'], columns=['eval_method','eval_measure'])
        piv.columns = [f'{i}_{j}' for i,j in piv.columns]
        piv.insert(loc=0, column='OS', value=piv.index)

        piv2 = pd.pivot_table(res, values='result', index=['os_method', 'eval_method'], columns=['eval_measure'])
        osvals = [x[0] for x in list(piv2.index)]
        methvals = [x[1] for x in list(piv2.index)]
        piv2.insert(loc=0, column='OS', value=osvals)
        piv2.insert(loc=1, column='ALG', value=methvals)

        ## Add best parameters
        naming_params = pd.Series(params_te.index).str.extract(r'(?P<ALG>.+)_(?P<OS>.+)_.*_.*')
        params_te2 = pd.DataFrame(params_te, columns=['best_params']).reset_index(drop=True)
        params_te2 = pd.concat([naming_params, params_te2], axis=1)


        ## parameters to df
        param_tab = load_dataset_params['make_params'].copy()
        param_tab['imb_ratio'] = round(calc_imb_ratio(DF), 1)
        param_tab['imb_ratio2'] = int(round(param_tab['weights'][0] / param_tab['weights'][1]))
        param_tab['weights'] = str(param_tab.get('weights'))
        param_tab1 = pd.DataFrame(pd.DataFrame.from_dict(param_tab, dtype='str', orient='index').transpose())
        param_tab1.insert(loc=9, column='n_informative2', value=param_tab['n_informative'] / param_tab['n_features'])
        param_tab2 = pd.DataFrame()
        for i in range(piv2.shape[0]):
            param_tab2 = pd.concat([param_tab2, param_tab1], axis=0)

        save_tab2 = pd.concat([param_tab2.reset_index(drop=True), piv2.reset_index(drop=True)], axis=1)
        save_tab2 = pd.merge(save_tab2, params_te2, on=['OS', 'ALG'], how='left')
        save_tab2.index = [cur_iter] * save_tab2.shape[0]


        with open(save_name2, 'a') as f:
            save_tab2.to_csv(f, header=f.tell() == 0, index=True)


        ## Print to logfile
        cur_data = f"{param_tab['n_features']}_{param_tab['imb_ratio2']}_{param_tab['n_clusters_per_class']}"
        cur_data = f"{cur_data}_{str(param_tab['n_informative'] / param_tab['n_features']).replace('.','')}"
        Logprint = f'[{cur_iter} / {total_iter}]\t{get_elapsed_time(start_time)}\t\t{cur_data}\n'
        make_log_entry(save_log_name, Logprint)


print('---------------\nTotal time:')
print(datetime.now().replace(microsecond=0) - start_time)

Logprint = f"\n\n---------------\nScript ends:\t{datetime.now().replace(microsecond=0)}"
make_log_entry(save_log_name, Logprint)
Logprint = f"\n---------------\nTotal Time:\t{get_elapsed_time(start_time)}"
make_log_entry(save_log_name, Logprint)









