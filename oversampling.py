
""" oversampling - functions to perform efficiently and parallelized oversampling with different classifiers

The main function is oversamp_comp0. Given an imbalanced dataset, a list of oversampling methods and a list of
classifiers among other options, oversampling is performed on the minority class of the dataset. For Random Forest and
Gradient Boosting best parameters can be obtained from a (random) grid search via cross validation. The final evaluation
of the oversampling strategies is conducted on a test set.
GAN oversampling can be done with a pretrained Generator or on the run where the best parameters for the neural network
are obtained from (randomized) experiments from a parameter grid that need to be passed. Note however, that, for
efficiency, it is very much recommended to train GANs on GPU while the classifiers rely on CPU. Unless this option is
used on a server that has enough capacities for both, these tasks - training and oversampling evaluation - should
better be separated so that pretrained Generators can be used.

Here is a brief overview of the additional helper functions in this script:

    * run_clf - the core function that runs the (parallelized) classifier training.
    * make_GAN_os_data - given the config (as dict), first GAN training is performed to obtain a trained Generator,
      then fake data is generated to conduct oversampling on the given dataset.
    * make_GAN_os_data2 - given a pretrained Generator, fake data is generated to conduct oversampling on the given
      dataset.
    * make_os_data - conduct oversampling, given an oversampling function from the imbalanced-learn package.
    * get_measure_and_res - calculate the desired score (e.g. accuracy, recall, ...) and add results to results df.
    * make_pivot_table - create a pivot table from the results dataframe for a better overview of the final results.
    * predict_fake - train classifier to predict whether data is real or fake (50% means not distinguishable).

"""




import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split, RandomizedSearchCV, ParameterGrid
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import RandomForestClassifier, GradientBoostingClassifier
from sklearn.metrics import accuracy_score, confusion_matrix, roc_curve, auc
from sklearn.metrics import precision_score, recall_score, f1_score, precision_recall_curve, average_precision_score
from sklearn.metrics import make_scorer
from imblearn.metrics import geometric_mean_score
from imblearn.over_sampling import SMOTE, BorderlineSMOTE, ADASYN, RandomOverSampler
import helpers
import torch
import multiprocessing
from sklearn.model_selection import ParameterSampler
from train_WGAN import train_WGAN
from train_cWGAN import train_cWGAN
from models import Generator
from datetime import datetime
from helpers import get_elapsed_time, make_log_entry
import importlib


# pd.set_option('display.max_columns', None, 'display.width', 160)



def oversamp_comp0(dataset, GANconfig=None, device=None, target='target', save_results='', cv_k=3, #G_unnormVECS=None,
                   test_size=0.2, os_methods=['gan'], eval_methods=['lr'], eval_measures=['all'],
                   random_seed=None,
                   rf_params=None, rf_grid=None, rf_grid_iter=10, gb_params=None, gb_grid=None, gb_grid_iter=10,
                   grid_verbose = 0, grid_scorer = 'f1_score',
                   num_cores=None, print_progress=False,
                   GANtype='cWGAN', GANloadParams=None, GANloadMinMax=None, GANpretrained=None,
                   return_conf=False, save_log = False, return_best_params=True):

    start_time = datetime.now().replace(microsecond=0)

    if num_cores is None: num_cores = multiprocessing.cpu_count()

    if 'rf' in eval_methods and (rf_params is None and rf_grid is None):
        raise Exception('Specify either rf_params or rf_grid to use RandomForest.')

    if 'gb' in eval_methods and (gb_params is None and gb_grid is None):
        raise Exception('Specify either gb_params or gb_grid to use GradientBoosting.')

    if ('gan' in os_methods or 'all' in os_methods) and GANconfig is None and GANpretrained is None:
        raise Exception('Specify GANconfig.')

    if device is None:
        device = torch.device('cpu')

    if save_log:
        save_log_name = f'{start_time.strftime("%Y-%m-%d_%H-%M-%S")}_oversampling.log'

    num_cores = min(multiprocessing.cpu_count(), num_cores)

    DF = dataset
    cols = list(DF.columns[~DF.columns.isin([target])])
    imb_ratio = round(DF[target].value_counts()[0] / DF[target].value_counts()[1], 4)

    X = DF.loc[:, DF.columns != target].reset_index(drop=True)
    y = DF.loc[:, DF.columns == target].reset_index(drop=True)

    res_te_all = pd.Series()
    best_params_all = pd.Series()


    if random_seed is not None:
        np.random.seed(random_seed)

    if rf_grid is not None:
        sampler = ParameterSampler(rf_grid, n_iter=rf_grid_iter)

    if gb_grid is not None:
        sampler = ParameterSampler(gb_grid, n_iter=gb_grid_iter)

    Xtr, Xte, ytr, yte = train_test_split(X, y, test_size=test_size, stratify=y)



    ###################
    ## OS strategies ##
    ###################
    osList = []

    ## No Oversampling
    if 'nos' in os_methods or 'all' in os_methods:
        Xtr_NOS, ytr_NOS = pd.DataFrame(Xtr), pd.DataFrame(ytr)
        osList += ['NOS']

    ## GAN
    if 'gan' in os_methods or 'all' in os_methods:
        if GANpretrained is None:
            Xtr_GAN, ytr_GAN = make_GAN_os_data(Xtr, ytr, GANconfig=GANconfig, gan=GANtype,
                                                loadParams=GANloadParams, loadMinMax=GANloadMinMax,
                                                device=device)
        else:
            Xtr_GAN, ytr_GAN = make_GAN_os_data2(Xtr, ytr, G=GANpretrained, device=device)
        osList += ['GAN']

    ## SMOTE
    if 'smote' in os_methods or 'all' in os_methods:
        os_fct = SMOTE(sampling_strategy='minority', n_jobs=num_cores, random_state=random_seed)
        Xtr_SMO, ytr_SMO = make_os_data(os_fct, Xtr, ytr)
        osList += ['SMO']

    ## Random Oversampling
    if 'ros' in os_methods or 'all' in os_methods:
        os_fct = RandomOverSampler(sampling_strategy='minority', random_state=random_seed)
        Xtr_ROS, ytr_ROS = make_os_data(os_fct, Xtr, ytr)
        osList += ['ROS']

    ## BorderlineSMOTE
    if 'bsmote' in os_methods or 'all' in os_methods:
        os_fct = BorderlineSMOTE(sampling_strategy='minority', n_jobs=num_cores, random_state=random_seed)
        Xtr_BSMO, ytr_BSMO = make_os_data(os_fct, Xtr, ytr)
        osList += ['BSMO']

    ## ADASYN
    if 'adasyn' in os_methods or 'all' in os_methods:
        os_fct = ADASYN(sampling_strategy='minority', n_jobs=num_cores, random_state=random_seed)
        Xtr_ADA, ytr_ADA = make_os_data(os_fct, Xtr, ytr)
        osList += ['ADA']



    #########################
    ## Logistic Regression ##
    #########################
    if 'lr' in eval_methods:
        clfname = 'LR'

        for osName in osList:
            if print_progress:
                Logprint = f'{clfname}_{osName}\t{get_elapsed_time(start_time)}'
                print(Logprint)
                if save_log:
                    make_log_entry(save_log_name, Logprint)

            cur_Xtr, cur_ytr = locals()[f'Xtr_{osName}'], locals()[f'ytr_{osName}']

            res_te = run_clf(Xtr=cur_Xtr, ytr=cur_ytr, Xte=Xte, yte=yte,
                             clfname=clfname, osName=osName, cv_k=None, eval_measures = eval_measures,
                             params=None, grid=None, grid_iter=1,
                             n_jobs=num_cores, return_conf=return_conf, grid_scorer=grid_scorer,
                             grid_verbose=0, return_best_params=False)

            res_te_all = pd.concat([res_te_all, res_te.mean(axis=0)], axis=0)



    ######################
    #### RandomForest ####
    ######################
    if 'rf' in eval_methods:
        clfname = 'RF'

        for osName in osList:
            if print_progress:
                Logprint = f'{clfname}_{osName}\t{get_elapsed_time(start_time)}'
                print(Logprint)
                if save_log:
                    make_log_entry(save_log_name, Logprint)

            cur_Xtr, cur_ytr = locals()[f'Xtr_{osName}'], locals()[f'ytr_{osName}']

            res_te, best_params = run_clf(Xtr=cur_Xtr, ytr=cur_ytr, Xte=Xte, yte=yte,
                                          clfname=clfname, osName=osName, cv_k=cv_k, eval_measures=eval_measures,
                                          params=rf_params, grid=rf_grid, grid_iter=rf_grid_iter,
                                          n_jobs=num_cores, return_conf=return_conf, grid_scorer=grid_scorer,
                                          grid_verbose=grid_verbose, return_best_params=True)

            res_te_all = pd.concat([res_te_all, res_te.mean(axis=0)], axis=0)
            best_params = pd.Series(data={f'{clfname}_{osName}_best_params': str(best_params)})
            best_params_all = pd.concat([best_params_all, best_params], axis=0)



    ##########################
    #### GradientBoosting ####
    ##########################
    if 'gb' in eval_methods:
        clfname = 'GB'

        for osName in osList:
            if print_progress:
                Logprint = f'{clfname}_{osName}\t{get_elapsed_time(start_time)}'
                print(Logprint)
                if save_log:
                    make_log_entry(save_log_name, Logprint)

            cur_Xtr, cur_ytr = locals()[f'Xtr_{osName}'], locals()[f'ytr_{osName}']

            res_te, best_params = run_clf(Xtr=cur_Xtr, ytr=cur_ytr, Xte=Xte, yte=yte,
                                          clfname=clfname, osName=osName, cv_k=cv_k, eval_measures=eval_measures,
                                          params=gb_params, grid=gb_grid, grid_iter=gb_grid_iter,
                                          n_jobs=num_cores, return_conf=return_conf, grid_scorer=grid_scorer,
                                          grid_verbose=grid_verbose, return_best_params=True)

            res_te_all = pd.concat([res_te_all, res_te.mean(axis=0)], axis=0)
            best_params = pd.Series(data={f'{clfname}_{osName}_best_params': str(best_params)})
            best_params_all = pd.concat([best_params_all, best_params], axis=0)


    if return_best_params:
        return res_te_all, best_params_all
    else:
        return res_te_all





def run_clf(Xtr, ytr, Xte, yte, clfname, osName, eval_measures,
            cv_k = None, params = None, grid = None, grid_iter = 1, n_jobs=-1,
            return_conf=False, grid_scorer='f1_score', grid_verbose = 0,
            return_best_params = False):

    if params is None and grid is None and clfname in ['RF', 'GB']:
        raise Exception("run_clf(): Specify either 'params' or 'grid'.")

    if cv_k is None: cv_k = 0

    if cv_k == 0 and return_best_params:
        raise Exception("run_clf(): 'return_best_params' does only make sense with 'cv_k'.")

    if clfname == 'LR':
        clf = LogisticRegression(solver='liblinear', C=10e20)
        cv_k = 0
    elif clfname == 'RF':
        if cv_k == 0:
            clf = RandomForestClassifier(n_jobs=n_jobs, **params)
        else:
            clf = RandomForestClassifier(n_jobs=n_jobs)
    elif clfname == 'GB':
        if cv_k == 0:
            clf = GradientBoostingClassifier(**params)
        else:
            clf = GradientBoostingClassifier()
    else:
        raise Exception("run_clf(): 'clfname' unknown.")


    res_te = pd.DataFrame()

    if cv_k == 0:
        clf.fit(Xtr, ytr.values.ravel())
        pred_te = clf.predict(Xte)
        pred_te = pd.DataFrame(pred_te, columns=[f'{clfname}_{osName}_pred'])
        proba_te = clf.predict_proba(Xte)
        proba_te1 = proba_te[:, 1]
        fpr_te, tpr_te, thres_te = roc_curve(yte, proba_te[:, 1])
        prpr_te, prrec_te, _ = precision_recall_curve(yte, proba_te[:, 1])

    elif cv_k > 1:
        module = importlib.import_module("sklearn.metrics")
        scorer_fct = make_scorer(getattr(module, grid_scorer))
        # scorer_fct = make_scorer(locals()[grid_scorer])
        n_iter = min(len(ParameterGrid(grid)), grid_iter)
        grid_search = RandomizedSearchCV(clf, grid, cv=cv_k, verbose=grid_verbose, n_jobs=n_jobs,
                                         scoring=scorer_fct, n_iter=n_iter)
        grid_search.fit(Xtr, ytr.values.ravel())

        pred_te = grid_search.predict(Xte)
        pred_te = pd.DataFrame(pred_te, columns=[f'{clfname}_{osName}_pred'])
        proba_te = grid_search.predict_proba(Xte)
        proba_te1 = proba_te[:, 1]
        fpr_te, tpr_te, thres_te = roc_curve(yte, proba_te[:, 1])
        prpr_te, prrec_te, _ = precision_recall_curve(yte, proba_te[:, 1])

        if return_best_params:
            best_params = grid_search.best_estimator_.get_params()
            best_params = {k: best_params[k] for k in list(grid.keys())}
            if clfname == 'GB':
                best_params['n_estimators_'] = grid_search.best_estimator_.n_estimators_

    else:
        raise Exception("run_clf(): 'cv_k' needs to be 0 or None or >= 2.")


    if 'acc' in eval_measures or 'all' in eval_measures:
        measure_fct = accuracy_score
        acc_te, res_te = get_measure_and_res(measure_fct, 'acc', yte, pred_te, res_te, osName, clfname)

    if 'recall' in eval_measures or 'all' in eval_measures:
        measure_fct = recall_score
        recall_te, res_te = get_measure_and_res(measure_fct, 'recall', yte, pred_te, res_te, osName, clfname)

    if 'precision' in eval_measures or 'all' in eval_measures:
        measure_fct = precision_score
        precision_te, res_te = get_measure_and_res(measure_fct, 'precision', yte, pred_te, res_te, osName, clfname)

    if 'f1_score' in eval_measures or 'all' in eval_measures:
        measure_fct = f1_score
        f1_te, res_te = get_measure_and_res(measure_fct, 'f1', yte, pred_te, res_te, osName, clfname)

    if 'g_mean' in eval_measures or 'all' in eval_measures:
        measure_fct = geometric_mean_score
        gm_te, res_te = get_measure_and_res(measure_fct, 'gmean', yte, pred_te, res_te, osName, clfname,
                                            add_params={'average': 'binary'})

    if 'auc' in eval_measures or 'all' in eval_measures:
        auc_te = auc(fpr_te, tpr_te)
        auc_te = pd.DataFrame([auc_te], columns=[f'{clfname}_{osName}_auc'])
        res_te = pd.concat([res_te, auc_te], axis=1)

    if 'aucpr' in eval_measures or 'all' in eval_measures:
        aucpr_te = auc(prrec_te, prpr_te)
        aucpr_te = pd.DataFrame([aucpr_te], columns=[f'{clfname}_{osName}_aucpr'])
        res_te = pd.concat([res_te, aucpr_te], axis=1)

    if 'ap' in eval_measures or 'all' in eval_measures:
        ap_te = average_precision_score(yte, proba_te1)
        ap_te = pd.DataFrame([ap_te], columns=[f'{clfname}_{osName}_ap'])
        res_te = pd.concat([res_te, ap_te], axis=1)

    if return_conf:
        TN_te, FP_te, FN_te, TP_te = confusion_matrix(yte, pred_te).ravel()
        conf_te = pd.DataFrame({f'{clfname}_{osName}_TN': [TN_te],
                                f'{clfname}_{osName}_FP': [FP_te],
                                f'{clfname}_{osName}_FN': [FN_te],
                                f'{clfname}_{osName}_TP': [TP_te]})
        res_te = pd.concat([res_te, conf_te], axis=1)

    if return_best_params:
        return res_te, best_params
    else:
        return res_te




def make_GAN_os_data(X, y, GANconfig, gan='cWGAN', random_seed=None, loadParams=None, loadMinMax=None, device=None):
    DF = pd.concat([X, y], axis=1)
    num_feat = X.shape[1]
    cols = list(X.columns)
    num_minority = y.target.value_counts()[0] - y.target.value_counts()[1]

    if loadParams is None:
        GANconfig['save_params'] = True
        load_path = GANconfig.get('all_save_path')
        if load_path is None: load_path = ''
        else: load_path = f'{load_path}/'
        load_params_name = f'{load_path}G_params.pt'
        load_minmax_name = f'{load_path}MinMax.npy'

        if gan == 'WGAN':
            train_WGAN(GANconfig, DF=DF)
            conditional = False
        elif gan == 'cWGAN':
            train_cWGAN(GANconfig, DF=DF)
            conditional = True

    else:
        load_params_name = loadParams
        load_minmax_name = loadMinMax

    G = Generator(ngpu=0, num_features=num_feat, **GANconfig['G'], conditional=conditional)
    G.load_state_dict(torch.load(load_params_name, map_location='cpu'))
    G.eval()

    G_unnormVECS = np.load(load_minmax_name)
    GAN = helpers.create_artificial_data(G=G, t1_size=num_minority, cols=cols, unnormVECS=G_unnormVECS,
                                         manual_seed=random_seed, device=device)
    X_GAN = GAN.drop('target', axis=1)
    y_GAN = pd.DataFrame(GAN['target'])
    X = pd.concat([X, X_GAN], ignore_index=True)
    y = pd.concat([y, y_GAN], ignore_index=True)

    return X, y



def make_GAN_os_data2(X, y, G, random_seed=None, device=None):
    cols = list(X.columns)
    num_minority = y.target.value_counts()[0] - y.target.value_counts()[1]
    GAN = helpers.create_artificial_data(G=G, t1_size=num_minority, cols=cols,
                                          manual_seed=random_seed, device=device)
    X_GAN = GAN.drop('target', axis=1)
    y_GAN = pd.DataFrame(GAN['target'])
    X = pd.concat([X, X_GAN], ignore_index=True)
    y = pd.concat([y, y_GAN], ignore_index=True)
    return X, y



def make_os_data(os_fct, X, y):
    _X, _y = os_fct.fit_resample(X, y.values.ravel())
    _X, _y = pd.DataFrame(_X), pd.DataFrame(_y)
    _X.columns, _y.columns = X.columns, y.columns
    return _X, _y



def get_measure_and_res(measure_fct, measure_name, y, y_pred, res, method_name, clf_name, add_params=None):
    if add_params is None:
        _mes = measure_fct(y.values.ravel(), y_pred)
    else:
        _mes = measure_fct(y.values.ravel(), y_pred, **add_params)
    _mes = pd.DataFrame([_mes], columns=[f'{clf_name}_{method_name}_{measure_name}'])
    _res = pd.concat([res, _mes], axis=1)
    return _mes, _res




def make_pivot_table(result):
    naming = pd.Series(result.index).str.extract(r'(?P<os_method>.+)_(?P<eval_method>.+)_(?P<eval_measure>.+)')
    res = pd.DataFrame(result, columns=['result'])
    res = pd.concat([res.reset_index(drop=True), naming.reset_index(drop=True)], axis=1)
    ret = pd.pivot_table(res, values='result', index=['os_method', 'eval_measure'], columns=['eval_method'])
    return ret






def predict_fake(DF, target='fake', eval_methods=['lr'], cv_k=3, random_seed=None, test_size=0.2,
                 rf_grid=None, rf_grid_iter=None, gb_grid=None, gb_grid_iter=None, grid_verbose=0,
                 grid_scorer='accuracy_score', return_conf=True, return_best_params=False, num_cores=None):

    if random_seed is not None:
        np.random.seed(random_seed)

    if num_cores is None: num_cores = multiprocessing.cpu_count()
    num_cores = min(multiprocessing.cpu_count(), num_cores)

    X = DF.loc[:, DF.columns != target].reset_index(drop=True)
    y = DF.loc[:, DF.columns == target].reset_index(drop=True)

    Xtr, Xte, ytr, yte = train_test_split(X, y, test_size=test_size, stratify=y)

    res_te_all = pd.Series()
    best_params_all = pd.Series()
    osName = 'GAN'


    if 'lr' in eval_methods:
        clfname = 'LR'
        res_te = run_clf(Xtr=Xtr, ytr=ytr, Xte=Xte, yte=yte,
                         clfname=clfname, osName=osName, cv_k=None, eval_measures=['acc'],
                         params=None, grid=None, grid_iter=1,
                         n_jobs=num_cores, return_conf=return_conf, grid_scorer=grid_scorer,
                         grid_verbose=grid_verbose, return_best_params=False)

        res_te_all = pd.concat([res_te_all, res_te.mean(axis=0)], axis=0)


    if 'rf' in eval_methods:
        clfname = 'RF'

        res_te, best_params = run_clf(Xtr=Xtr, ytr=ytr, Xte=Xte, yte=yte,
                                      clfname=clfname, osName=osName, cv_k=cv_k, eval_measures=['acc'],
                                      params=None, grid=rf_grid, grid_iter=rf_grid_iter,
                                      n_jobs=num_cores, return_conf=return_conf, grid_scorer=grid_scorer,
                                      grid_verbose=grid_verbose, return_best_params=True)

        res_te_all = pd.concat([res_te_all, res_te.mean(axis=0)], axis=0)
        best_params = pd.Series(data={f'{clfname}_{osName}_best_params': str(best_params)})
        best_params_all = pd.concat([best_params_all, best_params], axis=0)


    if 'gb' in eval_methods:
        clfname = 'GB'

        res_te, best_params = run_clf(Xtr=Xtr, ytr=ytr, Xte=Xte, yte=yte,
                                      clfname=clfname, osName=osName, cv_k=cv_k, eval_measures=['acc'],
                                      params=None, grid=gb_grid, grid_iter=gb_grid_iter,
                                      n_jobs=num_cores, return_conf=return_conf, grid_scorer=grid_scorer,
                                      grid_verbose=grid_verbose, return_best_params=True)

        res_te_all = pd.concat([res_te_all, res_te.mean(axis=0)], axis=0)
        best_params = pd.Series(data={f'{clfname}_{osName}_best_params': str(best_params)})
        best_params_all = pd.concat([best_params_all, best_params], axis=0)


    if return_best_params:
        return res_te_all, best_params_all
    else:
        return res_te_all













