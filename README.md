
# GAN Oversampling

## Short description
This project contains the python scripts I used for my Master thesis. Generative Adversarial Networks (GANs) are used to perform oversampling on imbalanced datasets to improve classification. In a simulation study GAN oversampling was compared to other oversampling strategies like SMOTE and ADASYN with different classifiers: Logistic Regression, Random Forest and Gradient Boosting. Parameters for the two latter were obtained by cross validation on a random grid. Evaluation was done on a test set using several measures: recall, precision, f1-score and AUC of the ROC-curve.



## Packages used
* The Neural Networks of the GANs were created with *pytorch*
* The experiments were conducted using *ray* for parallelization on CPU and GPU
* Plots were created with *seaborn*
* Classifiers and cross validation set up were used from *scikit-learn*
* Oversampling methods were used from *imbalanced-learn*
* Data handling and preprocessing was done with *pandas*
* For *R*-like Regression syntax *statsmodels* was used
* Further were *numpy* and *scipy* used for matrix operations, mathematical functions, etc.



## Project description 

### Introduction to topic
There are many cases where we want to identify a small subgroup in a dataset that is highly underrepresented. Examples are:
* Identify customers that are likely to respond to a marketing campaign
* Identify customers that have a high risk of credit default
* Identify fraudulent behavior
* Identify medical pictures that are conspicuous (e.g. tumors)

In all those examples we have two different classes where the class we are interested in (minority class) usually consists of much less observations than the other class (majority class). Such data is called imbalanced data and is well known to cause difficulties in the context of classification. Many algorithms are prone to favor the majority class and thus lead to poor performance in identifying minority class members. For intuition, consider a case where we have 99% majority observations and only 1% minority observations. An algorithm that just classifies all observations as majority class members will be correct in 99% of cases.

There are several strategies to tackle this problem. One is to oversample the minority class such that we obtain a (more) balanced dataset, e.g. with a ratio of 1:1. Popular strategies are to resample from the existing data or to use more sophisticated methods like SMOTE or ADASYN that are able to create new synthetic observations with simple algorithms.
However, due to the simplicity of these algorithms there may be shortcomings in the quality of the synthetic data, depending on the complexity of the underlying dataset.

After the great success of GANs -- especially in image processing -- the idea of using GANs to learn the distribution of the minority class and then to draw from the trained Generator to oversample the minority class with new (realistic) synthetic observations lies not far. And this is exactly what was done here. 


### Approach
The goal was to evaluate the performance of GAN oversampling compared to existing methods. For that purpose 
several synthetic datasets possessing different dataset characteristics (e.g. dimensionality, imbalance ratio) were used to set up a controlled simulation study.

Since GAN training is a time and resources consuming task a pre-selection of possibly interesting datasets settings has been done -- i.e. to find settings in which existing methods perform poorly. For this reason, 3000 experiments of comparing oversampling strategies together with different classifiers have been conducted, excluding GAN oversampling. With several Linear Regressions it was examined which dataset characteristics have an effect on the performance of the different oversampling methods. From this results the dataset characteristics *number of variables*, *imabalance ratio* and *dataset complexity* (in terms of subclusters wihtin each class) were chosen and datasets with combinations of these characteristics were created. This ended up in a set of rather *simple datasets*, with low values on the characteristics, and a set of rather *difficult datasets*.

On these datasets GANs - *conditional Wasserstein GANs* in particular - were trained. The trained Generators were then used to perform oversampling on the minority class and the results were compared to the other oversampling strategies.


### Results
On the simple datasets oversampling was not found to bring an advantage in many cases, so that it can be questioned if oversampling is appropriate at all in too simple settings.
On the other hand, for the difficult datasets oversampling becomes highly advisable, especially with an increasing difficulty of the dataset characteristics. Here, GAN oversampling showed strong performance on recall, though on high cost of precision. Also GAN oversampling shows a much more stable behavior with non-linear classifiers in increasingly difficult settings compared to the other oversampling methods. However, also SMOTE and ADASYN showed a good overall performance. And despite the advantages of GAN oversampling in many (especially complex) settings, it should be considered that GAN oversampling is a time consuming method. Thus, in praxis, GAN oversampling should be considered as a second step if simple oversampling strategies yield poor results.


## Notes
The scripts are not fully commented, since during the work on my thesis I wrote the comments for myself and they were not intended for other readers. I just removed most of it to clean up the scripts and added docstrings instead to each script with a short description of the script and its functions.
Also, the project description is of course only a very brief summary of what was done in detail.
So, if there are any questions please leave a comment in the *Questions* section under Issues or create a new issue.


