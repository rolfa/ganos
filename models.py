
"""models - Configurable Neural Networks classes for the Discriminator and the Generator

GANs consist of two neural networks - a Discriminator and a Generator. Each class is configurable w.r.t the number
and size of layers and whether a regular or conditional GAN is used.

"""



import torch
import torch.nn as nn
import torch.utils.data



class Discriminator(nn.Module):

    def __init__(self, ngpu, num_features, nDh0, D_add_layers=0, nDh1=0, nDh2=0, nDh3=0, conditional=False):
        super(Discriminator, self).__init__()
        self.ngpu = ngpu
        #self.lastDim = 0
        if conditional:
            ninput = num_features + 1
        else:
            ninput = num_features

        nDh_list = [nDh0, nDh1, nDh2, nDh3]

        main = nn.Sequential()
        main.add_module('hidden0:linear', nn.Linear(ninput, nDh0))
        main.add_module('hidden0:relu', nn.ReLU())
        lastDim = nDh0

        for i in range(D_add_layers):
            main.add_module(f'hidden{i+1}:linear', nn.Linear(lastDim, nDh_list[i+1]))
            main.add_module(f'hidden{i+1}:relu', nn.ReLU())
            lastDim = nDh_list[i+1]

        main.add_module('out:linear', nn.Linear(lastDim, 1))
        main.add_module('out:sigmoid', nn.Sigmoid())
        self.main = main


    def forward(self, X, c=None):
        x = X.view(X.size(0), -1)
        if c is not None:
            c = c.view(-1, 1).float()
            x = torch.cat((x, c), dim=1)
        y = self.main(x)
        return y



class Generator(nn.Module):

    def __init__(self, ngpu, nz, num_features, nGh0, G_add_layers=0, nGh1=0, nGh2=0, nGh3=0, conditional=False):
        super(Generator, self).__init__()
        self.ngpu = ngpu
        if conditional:
            self.ninput = nz + 1
        else:
            self.ninput = nz
        self.num_features = num_features
        nGh_list = [nGh0, nGh1, nGh2, nGh3]

        main = nn.Sequential()
        main.add_module('hidden0:linear', nn.Linear(self.ninput, nGh0))
        main.add_module('hidden0:relu', nn.ReLU())
        lastDim = nGh0

        for i in range(G_add_layers):
            main.add_module(f'hidden{i+1}:linear', nn.Linear(lastDim, nGh_list[i+1]))
            main.add_module(f'hidden{i+1}:relu', nn.ReLU())
            lastDim = nGh_list[i+1]

        main.add_module('out:linear', nn.Linear(lastDim, self.num_features))
        main.add_module('out:sigmoid', nn.Sigmoid())
        self.main = main


    def forward(self, z, c=None):
        x = z.float()
        if c is not None:
            c = c.view(-1,1).float()
            x = torch.cat((x,c), dim=1)
        y = self.main(x)
        return y




















































