
""" train_WGAN - callable function to perform Wasserstein GAN training

The function requires a config (as a dict) that specifies the parameters of the neural networks and the training
procedure. Can thus be use with ray tune to perform multiple experiments in an efficiently parallelized and organized
manner to find suitable architectures for both Discriminator and Generator as well as good parameters for the neural
networks and training procedure.
The function can run on CPU as well as on GPU.

"""



import numpy as np
import pandas as pd
from datetime import datetime
import torch.nn as nn
import torch.utils.data
import helpers
from models import Generator, Discriminator
from datasetloader import load_dataset
if __name__=="__main__":
    from oversampling import make_GAN_os_data2, run_clf




def train_WGAN(config, reporter=None, DF=None):

    if reporter is not None: from oversampling import make_GAN_os_data2, run_clf

    #### parameters
    batch_size = config['batch_size']
    print_progress = config['print_progress']
    nz = config['G']['nz']
    num_epochs = config["num_epochs"]
    k_critic = config['k_critic']
    num_workers = config['num_workers']
    ngpu = config['ngpu'] if torch.cuda.is_available() else 0

    fixed_noise_seed = np.random.randint(0,1000)

    save_params = config.get('save_params')
    if save_params is None: save_params = False

    all_save_path = config.get('all_save_path')
    if all_save_path is None or reporter is not None: all_save_path = ''
    else: all_save_path = f'{all_save_path}/'

    save_results = config.get('save_results')
    if reporter is None and save_results: save_res = pd.DataFrame()

    pause_G_start = config.get('pause_G_start')
    if pause_G_start is None: pause_G_start = 25
    pause_G_during = config.get('pause_G_during')
    if pause_G_during is None: pause_G_during = 500

    random_seed = config.get('random_seed')
    if random_seed is not None:
        np.random.seed(random_seed)
        torch.manual_seed(random_seed)


    #### device
    device = torch.device("cuda:0" if (torch.cuda.is_available() and ngpu > 0) else "cpu")


    #### dataset
    if DF is not None:
        DFtarget = DF['target']
        DF = DF.drop('target', axis=1)
        DF, minmax_vecs = helpers.scale_MinMax(data=DF, return_minmax=True, save_name=f'{all_save_path}MinMax.npy')
        DF['target'] = DFtarget
        DFdata = DF

    elif config['load_dataset'].get('test_split') is not None:
        DFdata, minmax_vecs, DFtest, minmax_vecs_test = load_dataset(normalize=True,
                                                                     return_norm_vec=True,
                                                                     vec_save=True,
                                                                     vec_save_path=all_save_path,
                                                                     **config['load_dataset'])
    else:
        DFdata, minmax_vecs = load_dataset(normalize=True,
                                           return_norm_vec=True,
                                           vec_save=True,
                                           vec_save_path=all_save_path,
                                           **config['load_dataset'])

    ## keep only Minorities
    if 'DFtest' in locals():
        DFfull = DFdata.copy()
    DFdata = DFdata[DFdata['target'] == 1]
    cols = list(DFdata.columns[~DFdata.columns.isin(['target'])])
    num_features = len(cols)
    t1_size = DFdata.shape[0]
    t0_size = 0


    ## Create Tensors ready to train
    data = torch.tensor(DFdata.drop('target', axis=1).values).float()


    ## dataloader
    dataloader = torch.utils.data.DataLoader(dataset=data,
                                             batch_size=batch_size,
                                             shuffle=True,
                                             num_workers=num_workers,
                                             drop_last=True)


    #### Specify NN models
    D = Discriminator(ngpu=ngpu,
                      num_features=num_features,
                      conditional = False,
                      **config['D']
                      ).to(device)

    G = Generator(ngpu=ngpu,
                  num_features=num_features,
                  conditional = False,
                  **config['G']
                  ).to(device)

    ## NN model init weights
    def init_weights(m):
        classname = m.__class__.__name__
        if classname.find('Linear') != -1:
            nn.init.normal_(m.weight.data, 0.0, 0.02)
            nn.init.constant_(m.bias.data, 0)


    ## Handle multi-GPU if available
    if (device.type == 'cuda') and (ngpu > 1):
        D = nn.DataParallel(D, list(range(ngpu)))
        G = nn.DataParallel(G, list(range(ngpu)))

    ## Optimizer
    Dopt = torch.optim.RMSprop(D.parameters(), lr=config["lr"])
    Gopt = torch.optim.RMSprop(G.parameters(), lr=config["lr"])

    ## Apply initial weights
    D.apply(init_weights)
    G.apply(init_weights)



    #### Training
    start_time = datetime.now().replace(microsecond=0)

    D_losses = []
    avg_JS = 1
    pause_iter = 0
    pause_G = pause_G_start
    errG = torch.tensor(99.0)
    stats_D_x, stats_D_G_z1, stats_D_G_z2 = torch.tensor(99.0), torch.tensor(99.0), torch.tensor(99.0)

    if print_progress:
        print(f'Starting Time:\t{start_time}\n')
        if pause_G_start > 0: (f'Pause G for the first {pause_G} iterations.')


    for epoch in range(num_epochs):
        D_losses_1ep = []
        for i, features in enumerate(dataloader):

            ## Parameter Clipping to enforce K-Lipschitz constraint
            for p in D.parameters():
                p.data.clamp_(min=-0.01, max=0.01)

            features = features.to(device)

            #### D real ####
            D.zero_grad()
            output_D_real = D(features)
            E_D_real = torch.mean(output_D_real)
            stats_D_x = E_D_real.item()

            #### D fake ####
            z = torch.randn(batch_size, nz, device=device)
            fake = G(z)
            output_D_fake = D(fake.detach())
            E_D_fake = torch.mean(output_D_fake)
            stats_D_G_z1 = E_D_fake.item()
            errD = E_D_fake - E_D_real
                                                                                                                        
            ## Update D
            errD.backward()
            Dopt.step()


            #### Pause G at the beginning and sometimes in between for 100 iter
            pause_iter += 1
            pause_G -= 1
            if pause_G_during > 0:
                if pause_iter % pause_G_during == 0:
                    pause_G = 100
                    if print_progress:
                        print(f'Pause G for {pause_G} iterations.')


            #### G ####
            if i % k_critic == 0 and pause_G < 0:
                G.zero_grad()
                output_G_fake = D(fake)
                errG = -torch.mean(output_G_fake)
                stats_D_G_z2 = -errG.item()

                ## Update G
                errG.backward()
                Gopt.step()

            D_losses_1ep.append(round(-errD.item(),30))

        ## After each epoch get avg of losses
        errD_avg = round(np.mean(D_losses_1ep),30)
        D_losses.append(abs(errD_avg))


        #### Gather and save statistics
        if (epoch + 1) % config['report_epoch_interval'] == 0:
            if reporter is None:
                if save_params:
                    torch.save(G.state_dict(), f'{all_save_path}G_params.pt')
                    torch.save(D.state_dict(), f'{all_save_path}D_params.pt')

                ## Check if the losses are changing
                lastentries = min(len(D_losses), 10)            ## last 10 epochs
                loss_change = -sum(D_losses[-lastentries:])
                if save_results or loss_change == 0:
                    ARTNdata = helpers.create_artificial_data(G=G, t0_size=t0_size, t1_size=t1_size, cols=cols,
                                                              device=device, manual_seed=fixed_noise_seed)

                    JS = []
                    for j in range(len(cols)):
                        cur_js = helpers.calc_distdist(DFdata.iloc[:, j], ARTNdata.iloc[:, j],
                                                       bins='freedman', measure="JS")
                        JS.append(cur_js)
                    avg_JS = round(np.mean(JS), 10)

                    if save_results:
                        cur_res = pd.DataFrame({'epoch': [epoch], 'JS': [avg_JS],
                                                'loss': [D_losses[-1]], 'loss_change': [loss_change]})
                        save_res = pd.concat([save_res, cur_res], axis=0)

                    if loss_change == 0:
                        print(f'loss_change == 0: exit;   JS: {avg_JS}')
                        if save_results:
                            save_res.to_csv(f'{all_save_path}results.csv')
                        if print_progress:
                            print('---------------\nTotal time:')
                            print(datetime.now().replace(microsecond=0) - start_time)
                        return


            elif reporter is not None:
                    ARTNdata = helpers.create_artificial_data(G=G,
                                                              t0_size=t0_size,
                                                              t1_size=t1_size,
                                                              cols=cols,
                                                              device=device,
                                                              manual_seed=fixed_noise_seed
                                                              )

                    JS = []
                    for j in range(len(cols)):
                        cur_js = helpers.calc_distdist(DFdata.iloc[:, j], ARTNdata.iloc[:, j],
                                                       bins='freedman', measure="JS")
                        JS.append(cur_js)

                    avg_JS = round(np.mean(JS), 10)

                    if errD_avg > 0:
                        Error_D = -errD_avg
                    else:
                        Error_D = errD_avg


                    ## Check if the losses are changing
                    lastentries = min(len(D_losses), 10)
                    loss_change = -sum(D_losses[-lastentries:])

                    ## In the end perform LR with GAN Oversampling on test set (if available)
                    if (loss_change == 0 or epoch >= num_epochs - 2) and 'DFtest' in locals():

                        _X, _y = make_GAN_os_data2(X = DFfull.drop('target', axis=1), y=DFfull[['target']], G=G,
                                                   random_seed=random_seed, device=device)

                        res_te = run_clf(Xtr=_X, ytr=_y, Xte=DFtest.drop('target', axis=1), yte=DFtest[['target']],
                                         clfname='LR', osName='GAN', n_jobs=1,
                                         eval_measures=['acc', 'precision', 'recall', 'f1_score', 'auc'])
                        res_te = res_te.mean(axis=0)

                    else:
                        res_te = {'LR_GAN_acc': 99,
                                  'LR_GAN_auc': 99,
                                  'LR_GAN_recall': 99,
                                  'LR_GAN_precision': 99,
                                  'LR_GAN_f1': 99,
                                  }


                    ## Save model parameters
                    if save_params:
                        torch.save(G.state_dict(), f'{all_save_path}G_params.pt')
                        torch.save(D.state_dict(), f'{all_save_path}D_params.pt')

                    n_informative2 = int(round(config['load_dataset']['make_params']['n_informative'] /
                                               config['load_dataset']['make_params']['n_features']))
                    imb_ratio2 = int(round(config['load_dataset']['make_params']['weights'][0] /
                                           config['load_dataset']['make_params']['weights'][1]))

                    reporter(mean_accuracy=-avg_JS,
                             loss_change = loss_change,
                             mean_loss=-Error_D,
                             avg_JS=avg_JS,
                             LR_acc = res_te['LR_GAN_acc'],
                             LR_auc = res_te['LR_GAN_auc'],
                             LR_recall = res_te['LR_GAN_recall'],
                             LR_precision = res_te['LR_GAN_precision'],
                             LR_f1 = res_te['LR_GAN_f1'],
                             n_features=config['load_dataset']['make_params']['n_features'],
                             n_clusters_per_class=config['load_dataset']['make_params']['n_clusters_per_class'],
                             n_informative2=n_informative2,
                             imb_ratio2=imb_ratio2,
                             )


            if print_progress:
                time_passed = datetime.now().replace(microsecond=0) - start_time
                print(f'{time_passed} [%d/%d]\t\tLoss_D: %.20f\tLoss_G: %.4f\tD(x): %.4f\tD(G(z)):' \
                       ' %.4f  / %.4f\t\tJS: %.3f'
                      % (epoch+1, num_epochs, -errD.data, -errG.data, stats_D_x, stats_D_G_z1, stats_D_G_z2, avg_JS))




    ## If model runs to the end, save last states
    if save_params:
        torch.save(G.state_dict(), f'{all_save_path}G_params.pt')
        torch.save(D.state_dict(), f'{all_save_path}D_params.pt')

    if save_results:
        save_res.to_csv(f'{all_save_path}results.csv')

    if print_progress:
        print('---------------\nTotal time:')
        print(datetime.now().replace(microsecond=0) - start_time)


