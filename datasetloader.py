
""" datasetloader - callable function to load and prepare specific datasets ready for use

By specifying the dataset name the function load_dataset returns the demanded dataset prepared ready for use.
Further options like normalizing or train/test splits can be set. Also simulated data can be created, where a dictionary
of parameters need to be passed via the make_params option.

"""


import os
import numpy as np
import pandas as pd
import helpers
from sklearn.datasets import make_classification
from sklearn.model_selection import train_test_split



def load_dataset(dataset, load_path='', normalize=True, return_norm_vec=False, vec_save=True, vec_save_path='',
                 make_params={}, test_split = None, split_seed = None):

    if normalize is False:
        return_norm_vec = False
        vec_save = False

    if load_path == '':
        load_path = f'{os.getcwd()}/datasets'

    if vec_save:
        slash = '/' if vec_save_path !='' else ''
        vec_save_name = f'{vec_save_path}{slash}MinMax.npy'
    else:
        vec_save_name = None


    if dataset == 'winequality':
        DF = pd.read_csv(f'{load_path}/winequality-white.csv', sep=';', header=0)
        DF['target'] = np.where(DF['quality'] <= 4, 1, 0)
        del DF['quality']
        DF.columns = [c.lower().replace(' ', '_') for c in DF.columns]


    elif dataset in ('make_classification', 'make'):

        if not make_params:                                                                                             ## note: empty dicts evaluates to False
            raise Exception("Specify 'make_params'")

        R = make_classification(**make_params)
        DF = pd.DataFrame(R[0])
        DF.columns = [f'var{i+1}' for i in range(DF.shape[1])]
        DF['target'] = R[1]


    elif dataset == 'bank':
        DF = pd.read_csv(f'{load_path}/bank.csv', sep=';', header=0, quotechar='\"')
        DF = DF.drop(['job', 'marital', 'education', 'contact', 'month'], axis=1)
        DF['default'].replace({'yes': 1, 'no': 0}, inplace=True)
        DF['housing'].replace({'yes': 1, 'no': 0}, inplace=True)
        DF['loan'].replace({'yes': 1, 'no': 0}, inplace=True)
        DF['poutcome'].replace({'unknown': 0, 'other': 0, 'failure': 0, 'success': 1}, inplace=True)
        DF['y'].replace({'yes': 1, 'no': 0}, inplace=True)
        DF.rename(columns={'y': 'target'}, inplace=True)


    elif dataset == 'bank-full':
        DF = pd.read_csv(f'{load_path}/bank-full.csv', sep=';', header=0, quotechar='\"')
        DF = DF.drop(['job', 'marital', 'education', 'contact', 'month'], axis=1)
        DF['default'].replace({'yes': 1, 'no': 0}, inplace=True)
        DF['housing'].replace({'yes': 1, 'no': 0}, inplace=True)
        DF['loan'].replace({'yes': 1, 'no': 0}, inplace=True)
        DF['poutcome'].replace({'unknown': 0, 'other': 0, 'failure': 0, 'success': 1}, inplace=True)
        DF['y'].replace({'yes': 1, 'no': 0}, inplace=True)
        DF.rename(columns={'y': 'target'}, inplace=True)


    elif dataset == 'abalone' or dataset == 'abalone_7':
        DF = pd.read_csv(f'{load_path}/abalone.csv', header=0, sep=',')
        DF['target'] = np.where(DF['rings'] == 7, 1, 0)
        DF.drop(['sex', 'rings'], axis=1, inplace=True)


    elif dataset == 'abalone_19':
        DF = pd.read_csv(f'{load_path}/abalone.csv', header=0, sep=',')
        DF['target'] = np.where(DF['rings'] == 19, 1, 0)
        DF.drop(['sex', 'rings'], axis=1, inplace=True)


    elif dataset == 'isolet':
        DF1 = pd.read_csv(f'{load_path}/isolet1+2+3+4.data', header=None, sep=',')
        DF2 = pd.read_csv(f'{load_path}/isolet5.data', header=None, sep=',')
        DF = pd.concat([DF1, DF2], axis=0, ignore_index=True)
        DF.columns = [f'f{i + 1}' for i in range(DF.shape[1])]
        DF['target'] = np.where(DF['f618'] <= 2, 1, 0)
        DF.drop(['f618'], axis=1, inplace=True)
        DF.dropna(inplace=True)


    ## https://archive.ics.uci.edu/ml/datasets/Letter+Recognition
    elif dataset == 'letter':
        DF = pd.read_csv(f'{load_path}/letter.csv', header=0, sep=',')
        DF['target'] = np.where(DF['lettr'] == 'Z', 1, 0)
        del DF['lettr']
        DF.columns = [c.lower().replace('-', '_') for c in DF.columns]


    elif dataset == 'letter_sample':
        DF = pd.read_csv(f'{load_path}/letter.csv', header=0, sep=',')
        DF = DF.sample(frac=0.5, replace=False, random_state=0)
        # DF.sample(frac=0.5, replace=False, random_state=0).to_csv(f'{load_path}/letter_sample.csv', index=False, sep=',', header=True)
        DF['target'] = np.where(DF['lettr'] == 'Z', 1, 0)
        DF.columns = [c.lower().replace('-', '_') for c in DF.columns]
        del DF['lettr']


    else:
        raise Exception('Dataset not available.')



    if test_split is not None and test_split > 0:
        # if split_seed is not None:
        #     np.random.seed(split_seed)
        DF, DFtest, t, ttest = train_test_split(DF.drop('target', axis=1), DF['target'], test_size=test_split,
                                                stratify=DF['target'], random_state=split_seed)
        DF['target'] = t
        DF = DF.reset_index(drop=True)
        DFtest['target'] = ttest
        DFtest = DFtest.reset_index(drop=True)


    if normalize:
        DFtarget = DF['target']
        DF = DF.drop('target', axis=1)
        DF, minmax_vecs = helpers.scale_MinMax(data=DF, save_name=vec_save_name, return_minmax=True)
        DF['target'] = DFtarget

        if test_split is not None and test_split > 0:
            DFtesttarget = DFtest['target']
            DFtest = DFtest.drop('target', axis=1)
            DFtest, minmax_vecs_test = helpers.scale_MinMax(data=DFtest, save_name=None, return_minmax=True)
            DFtest['target'] = DFtesttarget



    if not normalize and not (test_split is not None and test_split > 0):
        return DF
    elif normalize and not (test_split is not None and test_split > 0):
        return (DF, minmax_vecs) if (normalize and return_norm_vec) else DF
    elif not normalize and (test_split is not None and test_split > 0):
        return (DF, DFtest)
    elif normalize and (test_split is not None and test_split > 0):
        return (DF, minmax_vecs, DFtest, minmax_vecs_test) if (normalize and return_norm_vec) else (DF, DFtest, minmax_vecs_test)








































