
""" run_cWGAN_mc_grid - perform multiple experiments for each combination of dataset characteristics specified in a grid

For each element in a specified grid of combinations of dataset characteristics (number of features, imbalance ratio,
number of subclusters, number of informative variables) 200 experiments are performed on a random grid of training
parameters. The results of the experiments are collected and saved in an organized manner so that their evaluation can
be easily done afterwards. Experiments can be run on CPU or GPU (recommended for comprehensive experiments). The package
ray is used for parallelized experiments and an organized collection of the results. See their project page for details.

The following helper functions are used in this script:
    * trial_str_creator - use a specified logic to name the single experiments and experiment folders.
    * exp_name_creator - use a specified logic to name the whole experiment procedure for each combination.
    * make_weights_from_imbratio - transform a imbalance ratio to a pair of weights format, e.g.: 1:2 to (67,33).
    * make_int_ninf - transform the proportion of the number of informative variables to an integer.

"""


from train_cWGAN import train_cWGAN
import ray
from ray import tune
from datetime import datetime
import numpy as np
import re






script_start_time = datetime.now().replace(microsecond=0)
print(f'Script Starting Time:\t{script_start_time}\n')


tstamp = script_start_time.strftime("%Y-%m-%d_%H-%M-%S")
save_name = f"{tstamp}_train_cWGAN3.log"


def trial_str_creator(trial):
    pat = re.compile('(\d+)_*')
    trial_num = pat.search(trial.experiment_tag).group(1)
    return f'{trial.trainable_name}_{trial_num}_{trial.trial_id}'


def exp_name_creator(nf, ir, nc, ninf, layer = ''):
    ninf = str(ninf).replace('.','')
    if layer != '': layer = f'{layer}_'
    return f'train_cWGAN3_{layer}mc_{nf}_{ir}_{nc}_{ninf}'


def make_weights_from_imbratio(x):      ## make to weights by: for imb_ratio x = [x/(x+1)] / [(1/(x+1)]
    a = round(x/(x+1),4)
    b = round(1/(x+1),4)
    return (a,b)


def make_int_ninf(nf,ninf):
    return int(round(nf * ninf))


params = [ ## nf, ir, nc, ninf
    # [20,20,3,0.3],
    #[20,20,4,0.3],
    [20,20,5,0.3],
    [20,35,3,0.3],
    [20,35,4,0.3],
    [20,35,5,0.3],
    [20,50,3,0.3],
    [20,50,4,0.3],
    [20,50,5,0.3],
    [60,20,3,0.3],
    [60,20,4,0.3],
    [60,20,5,0.3],
    [60,35,3,0.3],
    # [60,35,4,0.3],
    # [60,35,5,0.3],
    [60,50,3,0.3],
    [60,50,4,0.3],
    [60,50,5,0.3],
    # [100,20,3,0.3],       ## 0.06
    [100,20,4,0.3],
    [100,20,5,0.3],
    [100,35,3,0.3],
    [100,35,4,0.3],
    [100,35,5,0.3],
    [100,50,3,0.3],
    [100,50,4,0.3],
    [100,50,5,0.3],
]


########################################################################################################################


iter = 0
leng = len(params)
for p in params:
    iter += 1

    nf = p[0]
    ir = p[1]
    nc = p[2]
    ninf = p[3]

    exp_start_time = datetime.now().replace(microsecond=0)
    print(f"[{iter} / {leng}]\t{exp_name_creator(nf, ir, nc, ninf, layer = '1L')}\tStarting Time:\t{exp_start_time}\n")


    ray.init()

    all_trials = tune.run(
        train_cWGAN,
        name=exp_name_creator(nf, ir, nc, ninf, layer = '1L'),
        config={
            'load_dataset': {
                'dataset' : 'make',
                # 'load_path': '~/Desktop/python/GAN/datasets/',
                'load_path': '/home/paperspace/datasets/',
                'test_split': 0.2,
                'split_seed': 1,
                'make_params': {
                    'n_samples':5000,
                    'n_features':nf,
                    'n_informative':make_int_ninf(nf, ninf),
                    'n_redundant':0,
                    'n_repeated':0,
                    'n_classes':2,
                    'weights':make_weights_from_imbratio(ir),
                    'n_clusters_per_class':nc,
                    'flip_y':0.01,
                    'hypercube':True,
                    'random_state':1,
                    # 'shuffle': False
                }
            },
            'resources': {
                # 'cpu': 32,
                'cpu': 8,
                'gpu': 1
            },
            'random_seed': 1,
            'print_progress':False,
            'save_params': True,
            'pause_G_start': 25,
            'pause_G_during': 500,
            'report_epoch_interval': 200,
            'num_workers': 1,
            'ngpu': 1,
            'num_epochs': 8000,
            'batch_size': tune.sample_from(lambda _: 50 * np.random.randint(1,7)),
            'k_critic': tune.sample_from(lambda _: np.random.randint(1,11)),
            'lr': tune.sample_from(lambda _: round(np.random.uniform(0.0001, 0.001),4)),
            'D': {
                'nDh0': tune.sample_from(lambda _: int(60 * 0.5 * np.random.randint(2,11))),
                'D_add_layers': 0,
                # 'nDh1': tune.sample_from(lambda _: np.random.randint(10,120)),
                #'nDh2': tune.sample_from(lambda _: np.random.randint(10,120)),
                #'nDh3': tune.sample_from(lambda _: np.random.randint(10,120))
            },
            'G': {

                'nz': tune.sample_from(lambda _: int(60 * 0.5 * np.random.randint(2,11))),
                'nGh0': tune.sample_from(lambda _: int(60 * 0.5 * np.random.randint(2,11))),
                'G_add_layers': 0,
                # 'nGh1': tune.sample_from(lambda _: np.random.randint(10,120)),
                #'nGh2': tune.sample_from(lambda _: np.random.randint(10,120)),
                #'nGh3': tune.sample_from(lambda _: np.random.randint(10,120))
            }
        },
        num_samples=200,
        stop={
            'loss_change': 0,
            'time_total_s': 3000
        },
        resources_per_trial={
            'cpu': 1,
            'gpu': 0.125
        },
        trial_name_creator=tune.function(trial_str_creator),
        reuse_actors=True,
        verbose=1,

    )



    ray.shutdown()



    ## Take logs about duration ofe ach exp
    exp_total_time = datetime.now().replace(microsecond=0) - exp_start_time
    with open(save_name, 'a') as f:
        f.write(f"[{iter} / {leng}]\t{exp_name_creator(nf,ir,nc, ninf,layer='1L')}\tTotal Exp Time:\t{exp_total_time}\n")


    print(f"\n\n---------------\n{exp_name_creator(nf, ir, nc, ninf, layer='1L')}\tTotal Exp Time:")
    print(exp_total_time)
    print("\n")





print('---------------\nTotal Script Time:')
print(datetime.now().replace(microsecond=0) - script_start_time)









